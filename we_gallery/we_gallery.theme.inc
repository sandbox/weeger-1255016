<?php
/**
 * @file
 * Define theme hooks and preprocess functions
 */

/**
 * Implementation of hook_theme().
 */
function we_gallery_theme() {

  $items = array();

  // Create an entry for each pager item
  $pagers = we_gallery_pagers_types();
  foreach ($pagers as $pager) {
    $items['we_gallery_pager-' . $pager] = array(
        'template' => 'we_gallery_pager-' . $pager,
        'arguments' => array('gallery' => NULL),
        'path' => drupal_get_path('module', 'we_gallery') . '/templates',
        'preprocess functions' => array('preprocess_we_gallery_pager_' . $pager),
    );
  }

  return $items;
}

/**
 * Theme preprocess function for views-view-we_gallery.tpl.php
 */
function template_preprocess_views_view_we_gallery(&$vars) {
  static $galleries_counter = 0;

  // Check the row plugin
  // If not present show an error message
  if (get_class($vars['view']->style_plugin->row_plugin) != 'views_plugin_row_we_gallery_snap') {
    drupal_set_message('WE Gallery : The views row plugin is not of type WE Gallery Snap', 'error');
    $vars['rows'] = array();
  }

  // Check if fields are filled
  if ($vars['view']->style_options['fields']['title_field'] == '' || $vars['view']->style_options['fields']['image_field'] == '') {
    drupal_set_message('WE Gallery : Specify witch field to use into Style options', 'error');
    return;
  }

  we_javascript_add_function('we_gallery_init');
  we_javascript_add_inline('jquery_easing');
  we_javascript_add_function('we_gallery_theme_' . $vars['options']['global']['theme']);
  // Add base CSS file
  we_javascript_add_css('we_gallery');
  we_javascript_add_css('we_gallery-theme-' . $vars['options']['global']['theme']);

  // Default gallery vars, encoded in JSON and sent to we_gallery javascript object
  // Get vars form views plugin and filter them to have
  // numbers for numeric values
  $gallery['view']['name'] = $vars['view']->name;
  $gallery['options'] = array_map("we_gallery_array_filter_number", $vars['options']);
  // Set a unique id
  $gallery['id'] = 'we_gallery_' . $galleries_counter++;
  // Get snaps
  $gallery['snaps'] = $vars['view']->snaps;

  // Render pagers
  if ($vars['options']['pagers']['sides'] == 1) {
    // If sides pagers, replace "sides" by "prev" and "next"
    $vars['options']['pagers']['arrow_prev'] =
            $vars['options']['pagers']['arrow_next'] = TRUE;
    unset($vars['options']['pagers']['sides']);
    // Preload we_javascript
    we_javascript_add_class('we_gallery_pager_arrow');
  }

  if ($vars['options']['pagers']['top'] == 1 || $vars['options']['pagers']['bottom'] == 1) {
    // Preload we_javascript
    we_javascript_add_class('we_gallery_pager');
  }

  foreach ($vars['options']['pagers'] as $pager => $active) {
    if ($active == TRUE) {
      // Render template
      $vars['pagers'][$pager] = theme('we_gallery_pager-' . $pager, $vars);
      // Add new vars to gallery datas
      $gallery['pagers'][$pager] = $vars['view']->pagers[$pager];
    }
  }

  // Default / No JS
  $gallery['default_gallery_height'] = $gallery['options']['snaps']['hover_height'];
  if ($gallery['options']['line']['mode'] == 'page') {
    $gallery['default_gallery_height'] *= $gallery['options']['line']['multiline_lines'];
  }

  // Save
  $vars['gallery'] = $gallery;
}

function preprocess_we_gallery_pager_top(&$vars) {
  $vars['type'] = 'top';
  preprocess_we_gallery_pager_pagers($vars);
}

function preprocess_we_gallery_pager_bottom(&$vars) {
  $vars['type'] = 'bottom';
  preprocess_we_gallery_pager_pagers($vars);
}

function preprocess_we_gallery_pager_pagers(&$vars) {

  if ($vars['gallery']['options']['line']['mode'] == 'page') {

    $vars['pages'] = _we_gallery_create_pages(
            ceil(count($vars['gallery']['rows']) / ($vars['gallery']['options']['snaps']['snaps_by_line'] * $vars['gallery']['options']['line']['multiline_lines'])), 1, $vars['gallery']['options']['line']['page_num_max']);
  }
  else {

    $vars['pages'] = _we_gallery_create_pages(
            count($vars['gallery']['rows']), $vars['gallery']['options']['snaps']['snaps_by_line'], $vars['gallery']['options']['line']['page_num_max']);
  }
}

/**
 * Return an array representing the pages with the index position of
 * each one, according to the max allowed pages number.
 */
function _we_gallery_create_pages($length, $items_by_page, $page_num_max) {

  $output = array();

  // Navigation range
  $range = ($length - $items_by_page);

  // Number of excpected pages
  $length_pages_full = ($page_num_max == 0) ? $length : $page_num_max;
  $length_pages_full -= 1;

  // Pages numbers can't be upper than snaps numbers
  if ($length_pages_full > $range) {
    $length_pages_full = $range;
  }

  if ($length_pages_full > 0) {

    // Check if all snaps are included
    $rest = $range % $length_pages_full;

    // Detect default gap between two pages numbers
    $step = ($range - $rest) / $length_pages_full;

    for ($i = 0; $i <= $length_pages_full; $i++) {
      $output[$i] = $i * $step;
    }
  }

  // Add rest to last page
  if ($rest > 0) {
    $output[$i - 1] = (($i - 1) * $step) + $rest;
  }

  return $output;
}

/**
 * Theme preprocess function for views-view-row-we_gallery_snap.tpl.php
 * @todo : Choose to use a field for the url in place of using node/id.
 */
function template_preprocess_views_view_row_we_gallery_snap(&$vars) {
  $index = $vars['view']->row_index;
  $snap['id'] = $vars['view']->name . '_snap_' . $index;
  $snap['nid'] = $vars['row']->nid;

  // Link
  if ($vars['view']->style_options['fields']['link_field'] != '') {
    $snap['url'] = $vars['view']->style_plugin->rendered_fields[$index][$vars['view']->style_options['fields']['link_field']];
  }
  else {
    // Use node path
    $snap['url'] = url('node/' . $vars['row']->nid);
  }

  // Link target
  $snap['target'] = '';
  if ($vars['view']->style_options['links']['target'] != '') {
    $snap['target'] = 'target="' . $vars['view']->style_options['links']['target'] . '"';
  }

  $snap['title'] = $vars['view']->style_plugin->rendered_fields[$index][$vars['view']->style_options['fields']['title_field']];
  $snap['image'] = $vars['view']->style_plugin->rendered_fields[$index][$vars['view']->style_options['fields']['image_field']];

  $vars['snap'] = $snap;
  // Set to view to transmit to gallery preprocess
  $vars['view']->snaps[] = $snap;
}

/**
 * Helpers
 */
function we_gallery_pagers_types() {
  return array('top', 'bottom', 'arrow_prev', 'arrow_next');
}

function we_gallery_array_filter_number($val) {
  if (is_numeric($val)) {
    return (int) intval($val);
  }
  elseif (is_array($val)) {
    return array_map("we_gallery_array_filter_number", $val);
  }

  return $val;
}
