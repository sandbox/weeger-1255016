return function(options) {

  $.extend(options, {
    border_sprite_width:300,
    border_offset_inner:30,
    snaps_border_width:40,
    snaps_border_offset:30,
    snaps_spacing_offset:-20,
    pager_arrows:{
      coords:{
        width:40
      }
    }
  });
	
}