return function(options) {
  $.extend(options,{
    border_sprite_width:300,
    border_offset_inner:50,
    snaps_border_width:15,
    snaps_border_offset:15,
    snaps_spacing_offset:0,
    pager_arrows:{
      coords:{
        width:40
      }
    }
  });	
}