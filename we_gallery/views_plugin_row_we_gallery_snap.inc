<?php
/**
 * @file
 * Views plugin definind the row style.
 */

/**
 * Implementation of views_row_plugin
 */
class views_plugin_row_we_gallery_snap extends views_plugin_row {

  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * Provide a form for setting options.
   */
  function options_form(&$form, &$form_state) {
    
  }

}
