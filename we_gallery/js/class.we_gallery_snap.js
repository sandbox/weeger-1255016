return $.inherit({
  __constructor:function(options) {
    
    var defaults = {
      id:null,
      index:null,
      title:'',
      url:'',
      offset:{},
      animate_now_prev:0,
      margin_top:0,
      margin_left:0,
      img_infos:null,
      waiting:true
    };
    
    $.extend(this,defaults);
    $.extend(this,options);
    
    var callback_this = this;
    
    // Add to index
    this.gallery.pages[this.page][this.id] =
    this.gallery.snaps[this.index] = this;
    
    // Get jQuery elements
    this.$ = $('#' + this.id, this.gallery.$);
    this.$_link = $('.we_gallery-snap-link:first',this.$);
    this.DOM_link = this.$_link.get(0);
    this.$_image = $('.we_gallery-snap-image:first',this.$);
    this.DOM_image = this.$_image.get(0);
    // Mask snap
    this.$
    .css('display','none')
    .css('position','absolute');
    
    this.$_link
    .css('position','absolute');
    
    // Get DOM elements, for a fastest manipulation
    this.DOM = $(this.$).get(0);
    this.DOM_corners = {
      tl:$('.corn-tl:first',this.$).get(0),
      tr:$('.corn-tr:first',this.$).get(0),
      br:$('.corn-br:first',this.$).get(0),
      bl:$('.corn-bl:first',this.$).get(0)
    };
    this.DOM_borders = {
      t:$('.border-t:first',this.$).get(0),
      r:$('.border-r:first',this.$).get(0),
      b:$('.border-b:first',this.$).get(0),
      l:$('.border-l:first',this.$).get(0)
    };
    
    // Add listeners
    
    // Create callback references
    // We need to have callback as vars, first to be able to bind and unbind them
    // second to not have anonymous function and keep "this" as reference
    this.callback_listener_mouseover = (function(cb) { return function() { cb.listener_mouseover(); } })(this);
    this.callback_listener_mouseout = (function(cb) { return function() { cb.listener_mouseout(); }; })(this);
    this.callback_listener_refresh_size = (function(cb) { return function() { cb.listener_refresh_size(); }; })(this);
    
    $('body').bind(this.gallery.id + '.refresh_size', this.callback_listener_refresh_size);
    
    // When pager moves, all snaps are disabled
    var callback_this = this;
    $('body').bind(this.gallery.id + '.goto',function(e,data) {
      callback_this.disable();
      callback_this.hover_out();
    });
    
    if (this.gallery.options.line.mode=='page') {
      this.index_top = Math.floor((this.index % (this.gallery.options.snaps.snaps_by_line * this.gallery.options.line.multiline_lines)) / this.gallery.options.snaps.snaps_by_line);
    }
    else {
      this.index_top = 0;
    }

    // Execute refresh size at startup
    this.listener_refresh_size();
    
  },
  
  /*
   * Lauch image loading to get image width / height infos
   * Add mouse listeners
   */
  enable:function() {
    this.enabled = true;
    
    this.$_image.css('background-image','url("' + $we.url(this.image,{file:true}) + '")');
    
    // Launch image loading
    var callback_this = this;
    if (this.gallery.options.snaps.hover_image_size==1) {
      $we.load_image($we.url(this.image),function(img) {
        callback_this.img_infos = img;
        callback_this.set_img_infos();
      });
    }
    
    // Trigger event on snap
    this.$_link
    .bind('mouseover',this.callback_listener_mouseover)
    .bind('mouseout',this.callback_listener_mouseout);

  },
  
  /*
   * Remove mouse listeners
   */
  disable:function() {
    this.enabled = false;

    // Trigger event on snap
    this.$_link
    .unbind('mouseover',this.callback_listener_mouseover)
    .unbind('mouseout',this.callback_listener_mouseout);

  },
  
  /*
   * Apply loaded width / height values to snaps
   * This function is called only one time for each snap when
   * image loading is complete AND when mouseout animation 
   * is complete too, in order to change position only in waiting mode.
   */
  set_img_infos:function() {
    if (this.img_infos!=null && this.waiting==true) {
      this.hover_width = this.img_infos.width;
      this.hover_height = this.img_infos.height;
      this.img_infos = null;
    }
  },
  
  listener_mouseover:function(){
    
    this.waiting = false;
    
    // Unbind to prevent multiple calls (IE)
    this.$_link.unbind('mouseover',this.callback_listener_mouseover);
    this.$_link.bind('mouseout',this.callback_listener_mouseout);
    
    this.hover = true;
    
    // Stop animation
    // animate_from and animate_to are used to start or stop
    // animating at the last point, not allways at the beginning or end
    // It's usefull when mouse in, out, then in again quickly before animation end up.
    this.animate_from = this.animate_now_prev;
    this.animate_to = 1;
    this.$.stop(true,true);
    this.$.addClass('we_gallery-snap-hover');
    
    // Reset margin size
    // Use a negative margin on mouse hover
    this.hover_margin_top = (this.default_height - this.hover_height)/2; 	
    this.hover_margin_left = (this.default_width - this.hover_width)/2;
    
    // Move DOM to the hover area
    this.hover_in();
    
    this.animate_now_prev = null;
    var callback_this = this;

    this.$.get(0).style.animate_var = 0;
    this.$
    .css('z-index','100')
    .animate({
      animate_var:1
    },
    {
      duration:this.gallery.options.animation.speed_hover_in,
      easing:this.gallery.options.animation.easing_hover_in,
      step:function(now,fx){
        callback_this.animate_step(now,fx);
      }
    });
  },
  
  listener_mouseout:function(){
    
    // Unbind to prevent multiple calls (IE)
    this.$_link.bind('mouseover',this.callback_listener_mouseover);
    this.$_link.unbind('mouseout',this.callback_listener_mouseout);
    
    this.hover = false;
    
    // Stop animation
    this.animate_from = 0;
    this.animate_to = this.animate_now_prev;
    this.$.stop(true,true);
    this.$.removeClass('we_gallery-snap-hover');
    
    this.animate_now_prev = 0;
    var callback_this = this;
    
    this.$.get(0).style.animate_var = 1;
    this.$
    .css('z-index','')
    .animate({
      animate_var:0
    },
    {
      duration:this.gallery.options.animation.speed_hover_out,
      easing:this.gallery.options.animation.easing_hover_out,
      step:function(now,fx){
        callback_this.animate_step(now,fx);
      },
      complete:function() { 
        // Replace DOM in the pager area
        callback_this.hover_out();
        this.waiting = true;
        callback_this.set_img_infos();
      }
    });
  },
  
  /**
   * Move to the "hover" div contaiter.
   */
  hover_in:function() {
    
    this.gallery.$_snaps_hover.append(this.$);

    if (this.gallery.options.line.mode=='page') {
      var index_left = (this.index) % (this.gallery.options.snaps.snaps_by_line);
    }
    else {
      var index_left = this.index;
    }
    
    // Manage index relatively to the current page offset
    index_left -= this.gallery.snap_active;
    
    this.offset.left = (this.default_width + this.gallery.options.snaps.spacing) * index_left;
    
    this.setup();
  },
  
  /**
   * Reset to the default status
   */
  hover_out:function() {
    
    if (this.gallery.options.line.mode=='page') { 
      var index_left = (this.index % this.gallery.options.snaps.snaps_by_line);
    }
    else {
      var index_left = this.index;
    }
    
    this.offset.left = (this.default_width + this.gallery.options.snaps.spacing) * index_left;
    
    this.margin = 0;
    this.gallery.$_snaps_inner.append(this.$);
    this.setup();
  },
  
  animate_step:function(now,fx){
    if (fx['prop']=='animate_var') {
      // Round "now" to improve performances
      now = Math.round((this.animate_from + ((this.animate_to - this.animate_from) * now)) * 100) / 100;
      
      // Change only when needed
      if (this.animate_now_prev!=now){
        this.animate_now_prev = now;
        this.margin_top = this.hover_margin_top * now;
        this.margin_left = this.hover_margin_left * now; 
        this.width = (this.default_width - (this.default_width * now)) + (this.hover_width * now);
        this.height = (this.default_height - (this.default_height * now)) + (this.hover_height * now);
        this.setup();
      }
    }
  },
  
  setup:function(){ 
    
    this.$.css({
      marginTop:this.margin_top + 'px',
      marginLeft:this.margin_left + 'px',
      left:this.offset.left,
      top:this.offset.top
    });

    // Link
    // If hover size is smallest as default size, keep it default size
    // and position during hover.
    if (this.hover_width<this.default_width) {
      this.DOM_link.style.width =  this.default_width + 'px';
      this.DOM_link.style.marginLeft = - this.margin_left + 'px';
    }
    else {
      this.DOM_link.style.width =  this.width + 'px';
    }
    
    if (this.hover_height<this.default_height) {
      this.DOM_link.style.height = this.default_height + 'px';
      this.DOM_link.style.marginTop = - this.margin_top + 'px';
    }
    else {
      this.DOM_link.style.height =  this.height + 'px';
    }
    
    // Image
    this.DOM_image.style.width = this.width + 'px';
    this.DOM_image.style.height = this.height + 'px';
    
    // Corners
    this.DOM_corners.br.style.top  =
      this.DOM_corners.bl.style.top  = (this.height - this.gallery.snaps_border_width + this.gallery.snaps_border_offset) + 'px';
    this.DOM_corners.br.style.left = 
      this.DOM_corners.tr.style.left = (this.width - this.gallery.snaps_border_width + this.gallery.snaps_border_offset) + 'px';
    
    this.DOM_corners.tl.style.top =
      this.DOM_corners.tl.style.left = 
      this.DOM_corners.tr.style.top =
      this.DOM_corners.bl.style.left = -(this.gallery.snaps_border_offset) + 'px';
    
    this.DOM_borders.t.style.top =
      this.DOM_borders.l.style.left = 0;
    
    // Borders
    this.DOM_borders.t.style.left =
      this.DOM_borders.b.style.left =
      this.DOM_borders.l.style.top =
      this.DOM_borders.r.style.top = (this.gallery.snaps_border_width - this.gallery.snaps_border_offset) + 'px';
    
    this.DOM_borders.t.style.top = 
      this.DOM_borders.l.style.left = 
      this.DOM_borders.b.style.bottom = 
      this.DOM_borders.r.style.right = -this.gallery.snaps_border_offset + 'px';
    
    this.DOM_borders.t.style.width =
      this.DOM_borders.b.style.width =  (this.width - (this.gallery.snaps_border_width * 2) + (this.gallery.snaps_border_offset * 2)) + 'px';
    this.DOM_borders.l.style.height = 
      this.DOM_borders.r.style.height = (this.height - (this.gallery.snaps_border_width * 2) + (this.gallery.snaps_border_offset * 2)) + 'px';
        
  },
  
  listener_refresh_size:function () {
    // Use default_width and height as reference
    // this.width and height can change during animations
    this.width = 
    this.default_width = this.gallery.snaps_width;
  
    this.height = 
    this.default_height = this.gallery.snaps_height;
  
    this.hover_width = this.gallery.snaps_hover_width;
    this.hover_height = this.gallery.snaps_hover_height;
    
    this.offset.top = (this.default_height + this.gallery.options.snaps.spacing) * this.index_top;

    this.setup();
  }
	
});