$we.load_class('we_gallery_snap');
$we.load_css('we_gallery');

return $.inherit({
  __constructor:function(options) {

    var defaults = {
      coords:{},
      snap_active:null,
      pages:[],
      page_active:null
    };

    $.extend(defaults,options);

    // Load theme datas
    // Force AJAX loading by using _call in place of call_function
    $we._call('function','we_gallery_theme_' + defaults.options.global.theme,[defaults]);
     
    // Apply default vars
    $.extend(this,defaults);
    
    this.pages_max = (this.options.line.mode=='line') ? this.options.snaps.snaps_by_line : this.pages_max;
    
    this.$ = $('#' + this.id);
    this.$.removeClass('we_gallery-nojs')
          .css('display','');
    this.$_snaps = $('.we_gallery_snaps',this.$);
    this.$_snaps_hover = $('.we_gallery_snaps-hover',this.$);
    this.$_snaps_inner = $('.we_gallery_snaps-inner',this.$);
    // Detect views editor path
    var views_reg = /^admin\/build\/views\/edit/;
    this.is_views_editor = (Drupal.settings.we_javascript.current_path.match(views_reg)) ? true : false;
    // Detect width
    this.base_width = (this.options.global.width!=0 && (!this.options.global.width_preview_only || this.is_views_editor)) ? this.options.global.width : this.$_snaps.width();
    this.options.snaps.spacing += (this.snaps_border_offset * 2) + (this.snaps_spacing_offset * 2);
    
    // Apply width on container
    this.$.height('inherit')
          .width(this.base_width);
    
    var pos_a = (this.border_sprite_width - (this.border_offset_inner + this.snaps_border_width));
    var pos_b = this.border_offset_inner;
    var theme_selector = ' .we_gallery-theme-' + this.options.global.theme;
    
    // Create style
    $('head').append('<style>'
    // Corners
    + theme_selector + ' .we_gallery-snap .corn-tl,'
    + theme_selector + ' .we_gallery-snap .corn-tr,'
    + theme_selector + ' .we_gallery-snap .corn-br,'
    + theme_selector + ' .we_gallery-snap .corn-bl {width:' + this.snaps_border_width + 'px; height:' + this.snaps_border_width + 'px;}'
    + theme_selector + ' .we_gallery-snap .corn-tl { background-position:-' + this.border_offset_inner + 'px -' + this.border_offset_inner + 'px; }'
    + theme_selector + ' .we_gallery-snap .corn-tr { background-position:-' + pos_a + 'px -' + pos_b + 'px; }'
    + theme_selector + ' .we_gallery-snap .corn-br { background-position:-' + pos_a + 'px -' + pos_a + 'px; }'
    + theme_selector + ' .we_gallery-snap .corn-bl { background-position:-' + pos_b + 'px -' + pos_a + 'px; }'
    // Borders
    + theme_selector + ' .we_gallery-snap .border-t, ' + theme_selector + ' .we_gallery-snap .border-b { height:' + this.snaps_border_width + 'px; }'
    + theme_selector + ' .we_gallery-snap .border-l, ' + theme_selector + ' .we_gallery-snap .border-r { width:' + this.snaps_border_width + 'px; }'
    + theme_selector + ' .we_gallery-snap .border-t { background-position:center -' + pos_b + 'px; }'
    + theme_selector + ' .we_gallery-snap .border-b { background-position:center -' + pos_a + 'px; }'
    + theme_selector + ' .we_gallery-snap .border-l { background-position:-' + pos_b + 'px center; }'
    + theme_selector + ' .we_gallery-snap .border-r { background-position:-' + pos_a + 'px center; }'
      
    + '</style>');
    
    // Construct pagers
    for (var i in this.pagers) {
      var options = {
        gallery:this,
        gallery_width_offset:0,
        type:i
      };
      
      $.extend(options,this.pagers[i]);
      
      switch(i) {
        case 'top' :
        case 'bottom' :
          $we._load('class','we_gallery_pager');
          new we_gallery_pager(options);
          break;
        case 'arrow_prev' :
        case 'arrow_next' :
          $we._load('class','we_gallery_pager_arrow');
          new we_gallery_pager_arrow(options);
          break;
      }
    }
    
    // Setup size
    this.refresh_size();
    
    // Get default selected image
    var default_nid = null;
    var default_snap = 0;
    var default_page = 0;
    if ('galleries_defaults' in Drupal.settings.we_gallery && this.view.name in Drupal.settings.we_gallery.galleries_defaults) {
      default_nid = Drupal.settings.we_gallery.galleries_defaults[this.view.name];
    }

    // Create snaps
    var pages_i = 0;	// Count number of element into a page
    var pages_num = 0;  // Current processed page
    for (var i in this.snaps) {

      // Create page container
      if (!(pages_num in this.pages)) { 
        this.pages[pages_num] = [];
      }
      
      if (this.snaps[i].nid == default_nid) {
        default_snap = parseInt(i);
        default_page = pages_num;
      }

      // Set default values
      var defaults = {
        gallery:this,
        title:'No title',
        page:pages_num,
        index:i
      };
      
      // Merge
      $.extend(defaults,this.snaps[i]);

      // Create snap
      new we_gallery_snap(defaults);

      pages_i++;

      // Manage page number
      if (this.options.line.mode=='page' && pages_i>=(this.options.snaps.snaps_by_line * this.options.line.multiline_lines)) {
        pages_i = 0;
        pages_num++;
      }
    }
    
    if (this.options.line.mode=='page') {
      // Go to the first page
      this.goto_page_num(default_page);
    }
    else {
      
      if (default_snap>this.snaps.length - this.options.snaps.snaps_by_line) {
        default_snap = this.snaps.length - this.options.snaps.snaps_by_line;
      }
      
      default_snap = (default_snap<0) ? 0 : default_snap; 
      
      // Prevent animation
      this.$_snaps_inner.css('margin-left',-(default_snap * (this.snaps_width + this.options.snaps.spacing)));
      // Go to the first item
      this.goto_snap_num(default_snap);
    }
    //d(this);
  },

  refresh_size:function() {
    
    // Detect width
    this.coords.width = this.base_width - this.options.snaps.spacing;
    // Substract offsets
    for (var i in this.pagers) {
      this.coords.width -= this.pagers[i].gallery_width_offset; 
    }
    // Set default snaps width
    this.snaps_width = ((this.coords.width) / this.options.snaps.snaps_by_line) - this.options.snaps.spacing;
    // Set snaps height if 0
    this.snaps_height = (this.options.snaps.height<=0) ? this.snaps_width : this.options.snaps.height;
    // Set hover sizes
    this.snaps_hover_width = this.options.snaps.hover_width;
    this.snaps_hover_height = (this.options.snaps.hover_square==1 || this.options.snaps.hover_height==0) ? this.options.snaps.hover_width : this.options.snaps.hover_height;
    
    // TODO : Load images and get sizes
    // Set height
    this.coords.height = this.snaps_height + this.options.snaps.spacing;
    // Multiline height
    if (this.options.line.mode=='page') {
      this.coords.height = (this.coords.height * this.options.line.multiline_lines);
    }
    
    // Trigger before applying size
    $('body').trigger('we_gallery.refresh_size',{gallery:this});

    this.$_snaps
    .width(this.coords.width + 'px')
    .height(this.coords.height + 'px')
    .css('padding',this.options.snaps.spacing + 'px 0 0 ' + this.options.snaps.spacing + 'px');

    this.$_snaps_hover
    .css('top', - this.coords.height)
    .css('left', this.options.snaps.spacing);
    
  },

  /**
   * Goto the requested command (prev, next, first, last),
   * depending to the we_gallery mode.
   */
  goto_action:function(command) {
    if (this.options.line.mode=='line') {
      this.goto_snap(command);
    }
    else {
      this.goto_page(command);
    }
  },

  /**
   * In line mode "next" say "the next snap"
   * The whole gallery stay on the same page (0).
   */
  goto_snap:function(command) {
    var snap = null;
    switch (command) {
      case 'first' :
        snap = 0;
        break;
      case 'prev' :
        if (this.snap_active > 0) {
          snap = this.snap_active - 1;
        }
        break;
      case 'next' :
        if (this.snap_active + this.options.snaps.snaps_by_line < this.snaps.length) {
          snap = new Number(this.snap_active) + 1;
        }
        break;
      case 'last' :
        snap = this.snaps.length - this.options.snaps.snaps_by_line;
        break;
      default :
        snap = new Number(command);
        break;
    }

    if (snap!==null) {
      this.goto_snap_num(snap);
    }
  },

  goto_snap_num:function(snap_num) {
    
    $('body').trigger(this.id + '.goto',{
      gallery:this,
      snap_num:snap_num,
      page_num:this.page_active
    });
    
    if (snap_num===this.snap_active || snap_num < 0 || (this.snap_num!=null && snap_num > (this.snaps.length-1))) {
      return;
    }
    var callback_this = this;

    var max_num = (snap_num - 1);
    max_num += (this.options.line.mode=='page') ? this.options.snaps.snaps_by_line * this.options.line.multiline_lines : this.options.snaps.snaps_by_line;
    
    var max_active = (this.snap_active + this.options.snaps.snaps_by_line - 1);
    
    for (var i in this.snaps) {
      
      // Snap come into the visible area
      if ((i>=snap_num && i <= max_num)
        /*||
        // Or is allready into the visible area (used the first time) / Disabled
        (i>=snap_num && i<this.snap_active) || (i<=max_num && i>max_active)*/) {
        this.snaps[i].$.css('display','');
      }
      callback_this.snaps[i].disable();
    }
    
    var callback_snap_num = snap_num;

    this.snap_active = snap_num;
    this.$_snaps_inner
    .stop()
    .animate({
      opacity:1,  // On startup, opacity can be set to 0 to animation
      marginLeft:-(snap_num * (this.snaps_width + this.options.snaps.spacing)),
      duration:this.options.animation.speed_goto,
      easing:this.options.animation.easing_goto
    },function() {

      for (var i in callback_this.snaps) {
        // Reactivate visible snaps
        if (i>=callback_snap_num && i <= max_num) {
          callback_this.snaps[i].enable();
        }
        // Hide out of screen
        else {
          callback_this.snaps[i].$.css('display','none');
        }
      }
    });
  },

  /**
   * In page mode, "next" say the next page.
   */
  goto_page:function(command) {
    var page = null;

    switch (command) {
      case 'first' :
        page = 0;
        break;
      case 'prev' :
        if (this.page_active > 0) {
          page = this.page_active - 1;
        }
        break;
      case 'next' :
        if (this.page_active < this.pages.length - 1) {
          page = new Number(this.page_active) + 1;
        }
        break;
      case 'last' :
        page = this.pages.length - 1;
        break;
      default :
        page = new Number(command);
        break;
    }

    if (page!==null) {
      this.goto_page_num(page);
    }
  },

  // Goto specified page
  // If loop is true, it means that the function is called by itself
  // after closing the current page
  goto_page_num:function(page_num,loop) {

    $('body').trigger(this.id + '.goto',{
      gallery:this,
      snap_num:this.snap_active,
      page_num:page_num
    });

    if (page_num===this.page_active || page_num < 0 || page_num > (this.pages_length-1)) {
      return;
    }

    var callback_this = this;
    
    // Disable all snaps
    for (var i in this.pages[callback_this.page_active]) { 
      this.pages[callback_this.page_active][i].hover_out();
      this.pages[callback_this.page_active][i].disable();
    }

    // Hide active page before opening new one
    if (this.page_active!==null && loop!=true) {

      var marginLeft = (page_num>this.page_active) ? -10 : 10 ;

      var animate_data = {
        opacity:0,
        marginLeft:marginLeft
      };

      this.$_snaps_hover.stop().animate({
        opacity:0
      },this.options.animation.speed_goto);
      
      this.$_snaps_inner.stop().animate(animate_data,this.options.animation.speed_goto,function() {
        // Hide snaps
        for (var i in callback_this.pages[callback_this.page_active]) { 
          callback_this.pages[callback_this.page_active][i].$.css('display','none');
        } 
        callback_this.goto_page_num(page_num,true);
      });
      
      return;
    }

    var marginLeft = (page_num>this.page_active) ? 10 : -10 ;
    this.$_snaps_inner.css('opacity','0').css('margin-left',marginLeft);

    var left_i = 0;
    var top_i = 0;
    for (var i in this.pages[page_num]) { 
      this.pages[page_num][i].setup();			
      this.pages[page_num][i].$.css('display','');
    }

    if (left_i>0) { top_i++; }

    this.page_active = page_num;

    this.$_snaps_inner.animate({
      opacity:1,
      marginLeft:0
    },this.options.animation.speed_goto,function() {
      callback_this.$_snaps_hover.css('opacity','');
      // Enable snaps
      for (var i in callback_this.pages[page_num]) { 
        callback_this.pages[page_num][i].enable();
      }
    });
  }
});