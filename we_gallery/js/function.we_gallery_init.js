return function() {
  $we.load_class('we_gallery');
  for (var i in Drupal.settings.we_gallery.galleries) {
    $we.call_inline('jquery_easing');
    new we_gallery(Drupal.settings.we_gallery.galleries[i]);
  };
}
