return $.inherit({
  __constructor:function(options) {
    var defaults = {
      coords:{
        width:20
      }	
    };
    
    $.extend(defaults,options); 
    // Get data form theme
    if ('pager_arrows' in options.gallery) {
      $.extend(defaults,options.gallery.pager_arrows);
    }
    $.extend(this,defaults);
    
    // Save to parent
    this.gallery.pagers[this.type] = this;
    // Get jQuery object
    this.$ = this.gallery.$.find('.we_gallery_pager-' + this.type + ':first');
    this.$_inner = this.$.find('.we_gallery_pager-page_num-inner:first');
    
    // Gallery width offset is substracted to the global gallery width
    this.gallery_width_offset = this.coords.width;

    if (this.type=='arrow_prev') {
      this.gallery.$_snaps.css('left',this.coords.width);
    }
    
    // Listeners  
    var callback_this = this;

    // Trigger click
    this.$.click(function() { 
      var page = null;
      callback_this.gallery.goto_action($(this).attr('rel'));
    });
    
    $('body').bind(this.gallery.id + '.goto',function(e,data) {
      callback_this.listener_goto(data.page_num,data.snap_num);
      callback_this.listener_refresh_size();
    });

},

listener_goto:function(page_num,snap_num) {
  
  if (this.gallery.options.line.mode=='page') {
    var num = page_num; 
    var num_max = this.gallery.pages.length - 1; 
  }
  else {
    var num = snap_num;
    var num_max = this.gallery.snaps.length - this.gallery.options.snaps.snaps_by_line; 
  }

  this.$.removeClass('disabled');
  
  // First page
  if (this.type=='arrow_prev' && num==0) {
    this.$.addClass('disabled');
  }
  // Last page
  else if (this.type=='arrow_next' && num >= num_max) {
    this.$.addClass('disabled');
  }
},
  
listener_refresh_size:function () {
  
  // Add an offset to the whole gallery for left arraow
  if (this.type=='arrow_prev') {
    this.gallery.$_snaps_hover.css('margin-left', this.coords.width + 'px');
  }
    
  this.coords.height = (this.gallery.coords.height + this.gallery.options.snaps.spacing);
    
  this.$
  .css({
    display:'block',
    marginTop:(- this.gallery.coords.height - this.gallery.options.snaps.spacing) + 'px'
  })
  .width(this.coords.width + 'px')
  .height(this.coords.height + 'px');
    
  this.$_inner.css('margin-top',(this.coords.height / 2) - (this.coords.width / 2) + 'px').css('background');
      
}
});