return $.inherit({
  __constructor:function(options) { 
    var callback_this = this;
    var defaults = {
      buttons:{}
    };
    
    $.extend(defaults,options);
    $.extend(this,defaults);
    
    // Save to parent
    this.gallery.pagers[this.type] = this;
		
    // Get DOM object
    this.$ = this.gallery.$.find('.we_gallery_pager-pager_' + this.type + ':first');
		
    // Save reference to jQuery objects of each button
    $('.we_gallery_pager-page_num, .we_gallery_pager-pager-arrow',this.$).each(function() {
      callback_this.buttons[$(this).attr('rel')] = $(this);
    });
    // Trigger clicks
    // Numbers
    $('.we_gallery_pager-page_num,.we_gallery_pager-pager-arrow',this.$).click(function() {
      callback_this.gallery.goto_action($(this).attr('rel'));
    });
    
    $('body').bind(this.gallery.id + '.goto',function(e,data) {
      callback_this.listener_goto(data.page_num,data.snap_num);
    });
  },
	
  reset:function() {
    for (var i in this.buttons) {
      this.buttons[i].removeClass('disabled').removeClass('active');
    }
  },
  
  listener_goto:function(page_num,snap_num) {
      
    if (this.gallery.options.line.mode=='page') {
      var num = page_num; 
      var num_max = this.gallery.pages.length - 1; 
    }
    else {
      var num = snap_num;
      var num_max = this.gallery.snaps.length - this.gallery.options.snaps.snaps_by_line; 
    }
    
    this.reset();
    
    if (num == 0) {
      this.buttons['first'].addClass('disabled');
      this.buttons['prev'].addClass('disabled');
    }
		
    if (num >= num_max) {
      this.buttons['next'].addClass('disabled');
      this.buttons['last'].addClass('disabled');
    }

    if (this.buttons[num]!=undefined) {
        this.buttons[num].addClass('active');
    }		

  },

});