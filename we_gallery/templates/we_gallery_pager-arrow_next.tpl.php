<?php
/**
 * @file
 * Template of the "next" arrow button.
 * Basically placed on the right side of the gallery.
 */
?>
<a class="we_gallery_pager-arrow we_gallery_pager-arrow_next" rel="next" href="javascript:void(0);">
  <div class="we_gallery_pager-page_num-inner">
   <div class="we_gallery_pager-page_num-text">Next page</div>
  </div>
</a>