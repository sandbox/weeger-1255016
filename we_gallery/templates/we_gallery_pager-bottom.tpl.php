<?php
/**
 * @file
 * Template of the pager block (start, previous, 1, 2 ..., next, end)
 * placed on the bottom of the gallery.
 * Pager top and pager bottom templates are the same html markups
 * except class attributes.
 */
?>
<div class="we_gallery_pager we_gallery_pager-pager we_gallery_pager-pager_bottom">
  <table>
    <tr>
      <td>
        <a class="we_gallery_pager-pager-arrow we_gallery_pager-pager-arrow_first" rel="first" href="javascript:void(0);">
          <span class="we_gallery_pager-page_num-text">&laquo;</span>
        </a>
      </td>
      <td>
        <a class="we_gallery_pager-pager-arrow we_gallery_pager-pager-arrow_prev" rel="prev" href="javascript:void(0);">
          <span class="we_gallery_pager-page_num-text">&lt;</span>
        </a>
      </td>
      <?php foreach ($pages as $page_number => $snap_number) : ?>
        <td>
          <a class="we_gallery_pager-page_num we_gallery_pager-page_num-<?php print $snap_number; ?>" rel="<?php print $snap_number; ?>"  href="javascript:void(0);">
            <span class="we_gallery_pager-page_num-text">
              <?php print($snap_number+1); ?>
            </span>
          </a>
        </td>
      <?php endforeach; ?>
      <td>
        <a class="we_gallery_pager-pager-arrow we_gallery_pager-pager-arrow_next" rel="next" href="javascript:void(0);">
          <span class="we_gallery_pager-page_num-text">&gt;</span>
        </a>
      </td>
      <td>
        <a class="we_gallery_pager-pager-arrow we_gallery_pager-pager-arrow_last" rel="last" href="javascript:void(0);">
          <span class="we_gallery_pager-page_num-text">&raquo;</span>
        </a>
      </td>
    </tr>
  </table>
</div>