<?php
/**
 * @file
 * Template of the "previous" arrow button.
 * Basically placed on the left side of the gallery.
 */
?>
<a class="we_gallery_pager-arrow we_gallery_pager-arrow_prev" rel="prev" href="javascript:void(0);">
  <div class="we_gallery_pager-page_num-inner">
   <div class="we_gallery_pager-page_num-text">Previous page</div>
  </div>
</a>