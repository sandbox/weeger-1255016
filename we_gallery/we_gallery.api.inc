<?php

/**
 * @file
 * Define function used by other modules
 */

/**
 * Defines witch image show first on page loading
 */
function we_gallery_set_startup_image($gallery_name, $nid) {
  drupal_add_js(array(
      'we_gallery' => array(
          'galleries_defaults' => array(
              $gallery_name => $nid
          )
      )
          ), 'setting');
}