<?php
/**
 * @file
 * Views plugin definind the WE Gallery style.
 */

/**
 * Implementation of views_plugin_style
 */
class views_plugin_style_we_gallery extends views_plugin_style {

  /**
   * Set default options
   * Snaps width is defines by lines_max * gallery width
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['global']['contains']['theme']['default'] = 'basic';
    $options['global']['contains']['width']['default'] = '0';
    $options['global']['contains']['width_preview_only']['default'] = TRUE;
    $options['snaps']['contains']['snaps_by_line']['default'] = 5;
    $options['snaps']['contains']['height']['default'] = 0;
    $options['snaps']['contains']['hover_width']['default'] = 200;
    $options['snaps']['contains']['hover_height']['default'] = 0;
    $options['snaps']['contains']['hover_image_size']['default'] = TRUE;
    $options['snaps']['contains']['spacing']['default'] = 10;
    $options['pagers']['contains']['top']['default'] = TRUE;
    $options['pagers']['contains']['bottom']['default'] = TRUE;
    $options['pagers']['contains']['sides']['default'] = TRUE;
    $options['animation']['contains']['speed_goto']['default'] = 500;
    $options['animation']['contains']['easing_goto']['default'] = 'easeOutQuad';
    $options['animation']['contains']['speed_hover_in']['default'] = 500;
    $options['animation']['contains']['easing_hover_in']['default'] = 'easeOutQuad';
    $options['animation']['contains']['speed_hover_out']['default'] = 500;
    $options['animation']['contains']['easing_hover_out']['default'] = 'easeOutQuad';
    $options['line']['contains']['mode']['default'] = 'line';
    $options['line']['contains']['page_num_max']['default'] = 10;
    $options['line']['contains']['multiline_lines']['default'] = 3;
    $options['fields']['contains']['image_field']['default'] = '';
    $options['fields']['contains']['title_field']['default'] = '';
    $options['fields']['contains']['link_field']['default'] = '';
    $options['links']['contains']['target']['default'] = '_self';

    return $options;
  }

  /**
   * Provide a form for setting options.
   * @param array $form
   * @param array $form_state
   */
  function options_form(&$form, &$form_state) {

    // Global
    $form['global'] = array(
        '#type' => 'fieldset',
        '#title' => t('Global'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE
    );
    
    // Theme
    // Scan all modules, search for folders prefixed with 'we_gallery-theme'
    $scan = we_register_scan_all('we_gallery-theme', array(
      'mode' => 'folder'
    ));

    foreach ($scan as $theme_name => $folder) {
      $themes[$theme_name] = $theme_name;
    }

    $form['global']['theme'] = array(
        '#type' => 'select',
        '#title' => t('Theme'),
        '#options' => $themes,
        '#default_value' => ($this->options['global']['theme']) ? $this->options['global']['theme'] : 'basic',
        '#description' => 'CCK field to use as an image field',
    );

    $form['global']['width'] = array(
        '#type' => 'textfield',
        '#title' => t('Width'),
        '#default_value' => $this->options['global']['width'],
        '#description' => 'Define fixed width for the gallery, 0 will adjust width to container (block, page, etc...)',
    );

    $form['global']['width_preview_only'] = array(
        '#type' => 'checkbox',
        '#title' => t('Fix width only for preview'),
        '#default_value' => $this->options['global']['width_preview_only'],
        '#description' => 'Check this box to use fixed width only on views preview (this page), width will be auto on other context.',
    );

    // Snaps
    $form['snaps'] = array(
        '#type' => 'fieldset',
        '#title' => t('Snaps'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE
    );

    $form['snaps']['snaps_by_line'] = array(
        '#type' => 'textfield',
        '#title' => t('Snaps by line'),
        '#default_value' => $this->options['snaps']['snaps_by_line'],
        '#description' => 'Snaps width will change automatically',
    );

    $form['snaps']['height'] = array(
        '#type' => 'textfield',
        '#title' => t('Snaps height'),
        '#default_value' => $this->options['snaps']['height'],
        '#description' => 'Height of each snap, leave to 0 will use same as width',
    );

    $form['snaps']['hover_width'] = array(
        '#type' => 'textfield',
        '#title' => t('Snaps width on mouse hover'),
        '#default_value' => $this->options['snaps']['hover_width'],
        '#description' => '',
    );

    $form['snaps']['hover_height'] = array(
        '#type' => 'textfield',
        '#title' => t('Snaps height on mouse hover'),
        '#default_value' => $this->options['snaps']['hover_height'],
        '#description' => '',
    );

    $form['snaps']['hover_image_size'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use image size on mouse hover'),
        '#default_value' => $this->options['snaps']['hover_image_size'],
        '#description' => 'Check this box will try to use the real images file dimension on mouse hover',
    );

    $form['snaps']['spacing'] = array(
        '#type' => 'textfield',
        '#title' => t('Snaps spacing'),
        '#default_value' => $this->options['snaps']['spacing'],
        '#description' => '',
    );

    // Pager
    $form['pagers'] = array(
        '#type' => 'fieldset',
        '#title' => t('Pagers'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE
    );

    $form['pagers']['top'] = array(
        '#type' => 'checkbox',
        '#title' => t('Head pager'),
        '#default_value' => $this->options['pagers']['top'],
        '#description' => 'Check this box will use same width and height on hover (ignoring previous field)',
    );

    $form['pagers']['bottom'] = array(
        '#type' => 'checkbox',
        '#title' => t('Footer pager'),
        '#default_value' => $this->options['pagers']['bottom'],
        '#description' => 'Check this box will use same width and height on hover (ignoring previous field)',
    );

    $form['pagers']['sides'] = array(
        '#type' => 'checkbox',
        '#title' => t('Sides pagers'),
        '#default_value' => $this->options['pagers']['sides'],
        '#description' => 'Check this box will use same width and height on hover (ignoring previous field)',
    );

    // Animation
    $form['animation'] = array(
        '#type' => 'fieldset',
        '#title' => t('Animation'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE
    );

    // Animation > Goto
    $form['animation']['speed_goto'] = array(
        '#type' => 'textfield',
        '#title' => t('Go to page / snap speed'),
        '#default_value' => $this->options['animation']['speed_goto'],
        '#description' => '',
    );

    $form['animation']['easing_goto'] = array(
        '#type' => 'select',
        '#title' => t('Go to page / snap animation easing'),
        '#options' => $this->easing_types(),
        '#default_value' => $this->options['animation']['easing_goto'],
        '#description' => '',
    );

    // Animation > Hover in
    $form['animation']['speed_hover_in'] = array(
        '#type' => 'textfield',
        '#title' => t('Mouse hover in speed'),
        '#default_value' => $this->options['animation']['speed_hover_in'],
        '#description' => '',
    );

    $form['animation']['easing_hover_in'] = array(
        '#type' => 'select',
        '#title' => t('Mouse hover in easing'),
        '#options' => $this->easing_types(),
        '#default_value' => $this->options['animation']['easing_hover_in'],
        '#description' => '',
    );

    // Animation > Hover out
    $form['animation']['speed_hover_out'] = array(
        '#type' => 'textfield',
        '#title' => t('Mouse hover out speed'),
        '#default_value' => $this->options['animation']['speed_hover_out'],
        '#description' => '',
    );

    $form['animation']['easing_hover_out'] = array(
        '#type' => 'select',
        '#title' => t('Mouse hover out easing'),
        '#options' => $this->easing_types(),
        '#default_value' => $this->options['animation']['easing_hover_out'],
        '#description' => '',
    );

    // Lines
    $form['line'] = array(
        '#type' => 'fieldset',
        '#title' => t('Lines'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE
    );

    $form['line']['mode'] = array(
        '#type' => 'radios',
        '#title' => t('Lines mode'),
        '#options' => array('line' => t('Single line'), 'page' => t('Multiline')),
        '#default_value' => $this->options['line']['mode'],
    );

    $form['line']['page_num_max'] = array(
        '#type' => 'textfield',
        '#title' => t('Maximum pages numbers'),
        '#default_value' => $this->options['line']['page_num_max'],
        '#description' => 'If pages or snaps number are too many, limit the displayed page number and go to a relative position',
    );

    $form['line']['multiline_lines'] = array(
        '#type' => 'textfield',
        '#title' => t('Lines (multiline mode only)'),
        '#default_value' => $this->options['line']['multiline_lines'],
        '#description' => 'Number of lines on each page',
    );

    // Fields
    $form['fields'] = array(
        '#type' => 'fieldset',
        '#title' => t('Fields'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE
    );

    // Fields list
    $field_names = array('' => '--');
    $handlers = $this->display->handler->get_handlers('field');

    foreach ($handlers as $field => $handler) {
      $field_names[$field] = $handler->ui_name();
      if ($label = $handler->label()) {
        $field_names[$field] .= " (\"$label\")";
      }
    }

    $form['fields']['image_field'] = array(
        '#type' => 'select',
        '#title' => t('Image field'),
        '#options' => $field_names,
        '#default_value' => $this->options['fields']['image_field'],
        '#description' => 'CCK field to use as an image field',
    );

    $form['fields']['title_field'] = array(
        '#type' => 'select',
        '#title' => t('Title field'),
        '#options' => $field_names,
        '#default_value' => $this->options['fields']['title_field'],
        '#description' => 'CCK field to use as title (alt)',
    );

    $field_names[''] = '- Use node path -';
    $form['fields']['link_field'] = array(
        '#type' => 'select',
        '#title' => t('Link field'),
        '#options' => $field_names,
        '#default_value' => $this->options['fields']['link_field'],
        '#description' => 'CCK field to use as link',
    );

    // Links
    $form['links'] = array(
        '#type' => 'fieldset',
        '#title' => t('Links'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE
    );

    // Animation > Hover in
    $form['links']['target'] = array(
        '#type' => 'textfield',
        '#title' => t('Target'),
        '#default_value' => $this->options['links']['target'],
        '#description' => 'Use html target value to define window destination : _blank, _parent, _self, etc... ',
    );
  }

  function easing_types() {
    return array(
        'swing' => 'swing',
        'easeInQuad' => 'easeInQuad',
        'easeOutQuad' => 'easeOutQuad',
        'easeInOutQuad' => 'easeInOutQuad',
        'easeInCubic' => 'easeInCubic',
        'easeOutCubic' => 'easeOutCubic',
        'easeInOutCubic' => 'easeInOutCubic',
        'easeInQuart' => 'easeInQuart',
        'easeOutQuart' => 'easeOutQuart',
        'easeInOutQuart' => 'easeInOutQuart',
        'easeInQuint' => 'easeInQuint',
        'easeOutQuint' => 'easeOutQuint',
        'easeInOutQuint' => 'easeInOutQuint',
        'easeInSine' => 'easeInSine',
        'easeOutSine' => 'easeOutSine',
        'easeInOutSine' => 'easeInOutSine',
        'easeInExpo' => 'easeInExpo',
        'easeOutExpo' => 'easeOutExpo',
        'easeInOutExpo' => 'easeInOutExpo',
        'easeInCirc' => 'easeInCirc',
        'easeOutCirc' => 'easeOutCirc',
        'easeInOutCirc' => 'easeInOutCirc',
        'easeInElastic' => 'easeInElastic',
        'easeOutElastic' => 'easeOutElastic',
        'easeInOutElastic' => 'easeInOutElastic',
        'easeInBack' => 'easeInBack',
        'easeOutBack' => 'easeOutBack',
        'easeInOutBack' => 'easeInOutBack',
        'easeInBounce' => 'easeInBounce',
        'easeOutBounce' => 'easeOutBounce',
        'easeInOutBounce' => 'easeInOutBounce'
    );
  }

}