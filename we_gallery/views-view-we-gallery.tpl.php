<?php
/**
 * @file
 * Template of one gallery.
 * This include the rendered content of all thumbnails.
 */
?>
<div id="<?php print $gallery['id']; ?>" class="we_gallery we_gallery-nojs we_gallery-theme-<?php print $gallery['options']['global']['theme'] ?>" style="height:<?php print $gallery['default_gallery_height']; ?>px;">
  <?php if (isset($pagers['top'])) : ?>
    <?php print $pagers['top']; ?>
  <?php endif; ?>
  <div class="we_gallery_snaps">
    <div class="we_gallery_snaps-inner">
      <?php foreach ($rows as $id => $row): ?>
        <?php print $row; ?>
      <?php endforeach; ?>
    </div>
  </div>
  <div class="we_gallery_snaps-hover"></div>
  <?php if (isset($pagers['arrow_prev'])) : ?>
    <?php print $pagers['arrow_prev']; ?>
  <?php endif; ?>
  <?php if (isset($pagers['arrow_next'])) : ?>
    <?php print $pagers['arrow_next']; ?>
  <?php endif; ?>
  <?php if (isset($pagers['bottom'])) : ?>
    <?php print $pagers['bottom']; ?>
  <?php endif; ?>
  <noscript>
    <style>
      #<?php print $gallery['id']; ?> .we_gallery-snap { float:left; }
      #<?php print $gallery['id']; ?> .image-nojs { width:<?php print $gallery['options']['snaps']['hover_height']; ?>px; height:<?php print $gallery['options']['snaps']['hover_height']; ?>px; }
    </style>  
    <div class="messages we_gallery-message-nojs">
      <?php print t('You must enable Javascript to view this gallery in a better display.'); ?>
    </div>
  </noscript>
</div>
<script type="text/javascript">
  // Mask gallery before loading if JS is enabled
  document.getElementById('<?php print $gallery['id']; ?>').style.display = "none";
  // Add to settings, used for galleries detection
  Drupal.settings.we_gallery.galleries['<?php print $gallery['id']; ?>'] = <?php print json_encode($gallery); ?>;
</script>
<?php

// Reload $we javascript functions
// Used into views on reloading preview.
print we_javascript_footer_ajax();