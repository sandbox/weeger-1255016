<?php
/**
 * @file
 * Define views plugins
 */

/**
 * Implementation of hook_views_plugins().
 */
function we_gallery_views_plugins() {
  return array(
      'style' => array(//declare the views_json style plugin
          'we_gallery' => array(
              'title' => t('WE Gallery'),
              'theme' => 'views_view_we_gallery',
              'help' => t('Displays nodes in the WE gallery interface.'),
              'handler' => 'views_plugin_style_we_gallery',
              'uses row plugin' => TRUE,
              'uses fields' => TRUE,
              'uses options' => TRUE,
              'type' => 'normal',
          ),
      ),
      'row' => array(//declare the unformatted row plugin
          'we_gallery_snap' => array(
              'title' => t('WE Gallery snap'),
              'help' => t('(Displays the unformatted data for each row from the views query with each row on a new line. Set as | for views_json.'),
              'handler' => 'views_plugin_row_we_gallery_snap',
              'theme' => 'views_view_row_we_gallery_snap',
              'uses fields' => TRUE,
              'uses options' => TRUE,
              'type' => 'normal',
          )
      ),
  );
}
