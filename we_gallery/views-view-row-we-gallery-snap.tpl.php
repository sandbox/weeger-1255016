<?php
/**
 * @file
 * Template of one image "snap" (thumbnail)
 * <img> content is only renered when javascript is disabled,
 * this prevent loading all the gallery images on page startup
 * and keep search engines optimisation.
 */
?>
<div id="<?php print $snap['id']; ?>" class="we_gallery-snap we_gallery-snap-nojs">
  <div class="we_gallery-snap-image"></div>
  <span class="border-t"></span>
  <span class="border-r"></span>
  <span class="border-b"></span>
  <span class="border-l"></span>
  <span class="corn-tl"></span>
  <span class="corn-tr"></span>
  <span class="corn-br"></span>
  <span class="corn-bl"></span>
  <a title="<?php print $snap['title']; ?>" class="we_gallery-snap-link" href="<?php print $snap['url']; ?>" <?php print $snap['target']; ?>>
    <noscript>
      <div class="image-nojs"><img src="<?php print url($snap['image']); ?>" /></div>
    </noscript>
  </a>
</div>