<?php
/*
 * @file
 *
 * Defines here list of loaders functions. The list should be the same as
 * the load_xxx functions into $we object in javascript. If a function exists
 * in javascript to load something, the same function must exist in PHP to
 * load the same thing directly on page startup.
 *
 * > function
 * > class
 * > jquery
 * > inline
 * > css
 * > image
 */

function _we_javascript_add($function_type, $function_name) {
  // Get script
  // All sub calls are included
  // All data are saved into we_javascript_output_array()
  $function = 'we_javascript_add_' . $function_type;
  if (function_exists($function)) {
    return $function($function_name);
  }
  return FALSE;
}

/**
 * Load specified javascript function and add it to the output array
 * Invoke all modules if they want to put some other script files
 * depending of script name or type.
 */
function _we_javascript_add_script($function_name, $function_type, $parse=TRUE) {

  $functions = we_javascript_get_scripts();

  // If the fonction is not allready loaded
  if (!we_javascript_output_array($function_type, $function_name)
          && $functions[$function_type][$function_name]
          && file_exists($functions[$function_type][$function_name])) {

    // Set we_javascript_output to true before function execution to define
    // this function as registered and prevent recursions if a sub function
    // call it once again
    we_javascript_output_array($function_type, $function_name, TRUE);

    // Load file content
    $script = file_get_contents($functions[$function_type][$function_name]);
    if (!$script) {
      return FALSE;
    }

    $output = '';

    // Parsing can be disabled if script need to be loaded after page startup
    if ($parse) {
      // Get types using $we.call_* and $we.load_* function
      $call_types = array();

      foreach (we_javascript_js_call_types() as $type) {
        $match_tyes['call_' . $type] = $type;
      }

      foreach (we_javascript_js_load_types() as $type) {
        $match_types['load_' . $type] = $type;
      }

      // Search for load_* calls into script and add it to the output array
      foreach ($match_types as $we_function => $type) {
        // Get calls to load_function() to include called function into returned content
        preg_match_all("`we." . $we_function . "\(\'(.*)\'`", $script, $results_load);
        // Load requested functions
        foreach ($results_load[1] as $import) {
          _we_javascript_add($type, $import);
        }
      }

      // Search for jQuery plugins
      preg_match_all('`\$\(.*\)\.(.*)\(`', $script, $results);
      foreach ($results[1] as $plugin_name) {
        _we_javascript_add_script($plugin_name, 'jquery');
      }
    }

    // Add to output
    we_javascript_output_array($function_type, $function_name, $function_name . ':function(){' . $script . '}');
  }
  // Reset content if allreadey exists
  // Si action will put the script data to the top the queue before parsing
  elseif (we_javascript_output_array($function_type, $function_name)) {
    we_javascript_output_array($function_type, $function_name, we_javascript_output_array($function_type, $function_name));
  }
}

/**
 * Add a javascript function to the output array
 */
function we_javascript_add_function($name, $parse = TRUE) {
  _we_javascript_add_script($name, 'function', $parse);
}

/**
 * Add a javascript class to the output array
 */
function we_javascript_add_class($name, $parse = TRUE) {
  _we_javascript_add_script($name, 'class', $parse);
}

/**
 * Add a jquery function to the output array
 */
function we_javascript_add_jquery($name, $parse = TRUE) {
  _we_javascript_add_script($name, 'jquery', $parse);
}

/**
 * Add inline javascript from a file
 */
function we_javascript_add_inline($name, $parse = TRUE) {
  static $inline_counter = 0;
  $scripts = we_javascript_get_scripts();

  // Check if name is a key name of an inline.name.js file
  if (isset($scripts['inline'][$name])) {
    _we_javascript_add_script($name, 'inline', $parse);
    return;
  }
  // Check if name is a full file path
  elseif (file_exists($name)) {
    $script = file_get_contents($name);
    $name = preg_replace('/[^a-z0-9]/i', '_', $name);
  }
  // Consider $parse as inline javascript
  else {
    $script = $parse;
  }

  if (isset($script)) {
    // Add to output
    we_javascript_output_array('inline', $name, $name . ':function(){' . $script . '}');
  }
}

/**
 * Add a javascript css to the output array.
 * The file is parsed and images files are loaded by $we.
 * Get the file content and convert relative path to absolute urls
 * @todo : Use $(document).add_js_link() when images are loaded.
 */
function we_javascript_add_css($name) {
  $scripts = we_javascript_get_scripts();
  $url = $scripts['css'][$name];

  if (file_exists($url)) {
    global $base_path;

    // Add to drupal
    drupal_add_css($url);

    // Extract images
    $content = file_get_contents($url);
    $content = str_replace(array("\r\n", "\r", "\n", "\t"), ' ', $content);
    $output = array();

    preg_match_all('/(.+?)\s?\{\s?(.+?)\s?\}/', $content, $results);

    foreach ($results[1] as $key => $selector) {
      $styles = explode(';', $results[2][$key]);

      foreach ($styles as $style) {
        $exp = explode(':', $style);
        $style_key = trim($exp[0]);
        $style_value = trim($exp[1]);
        if ($style_key != '' && $style_value != '') {

          if (strpos($style_value, 'url') !== FALSE) {
            preg_match('/url\(\s?(.+?)\s?\)/', $style_value, $res);
            if (isset($res[1])) {
              // Remove quotes
              $url_relative = trim($res[1], '"\'');
              // Resolve
              $url_absolute = we_javascript_resolve_path(url($url), $url_relative);
              // Replace
              $style_value = str_replace($res[1], "'" . $url_absolute . "'", $style_value);
              // Add to image loading queue
              we_javascript_add_image($url_absolute);
            }
          }
        }
      }
    }

    we_javascript_output_array('css', $name, $url);
  }
}

function we_javascript_add_image($url) {
  // Get existing images
  $images = we_javascript_output_array('image', 'all');
  $images = (!is_array($images)) ? array() : $images;
  $images[] = $url;
  we_javascript_output_array('image', 'all', $images);
}

function we_javascript_add_string($name, $string) {
  we_javascript_output_array('string', $name, $string);
}