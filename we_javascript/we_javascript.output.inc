<?php
/**
 * @file
 * Functions used to render and pack javascript
 */

/**
 * Save files contents in a temporary array before processing output
 * Return parts of the array if no content ($script) is defined.
 * This array is returned as a json object and interpreted by the parser.
 */
function we_javascript_output_array($function_type = NULL, $function_name = NULL, $script = NULL) {
  static $output = array();

  if ($function_type !== NULL) {
    if ($function_name !== NULL) {
      if ($script !== NULL) {
        if ($script === $output[$function_type][$function_name]) {

          /* If the content allready exists, unset and reset it,
           * this action put the called script to the top of the
           * loading queue (the output array is reverted before
           * transmiting to js). So javascript caller will load
           * this script before all previous ones.
           * This prevent parser to reload scripts by ajax during
           * the parsing process, before all scripts definition
           * are completly loaded.
           * Here, the last loaded function is the orginial called one.
           */

          unset($output[$function_type][$function_name]);
          $output[$function_type][$function_name] = $script;
        }
        else {
          $output[$function_type][$function_name] = $script;
          // If script is TRUE, the function is not completly registered
          // Only saved to prevent recursions on parsing.
          // We wait complete loading before invoking hook.
          if ($script !== TRUE) {
            // Hook all modules
            // Can be used for example if a module want to add a css file
            // into the we_javascript response, or execute a js file on calling css.
            module_invoke_all('we_javascript_add_script', $function_type, $function_name, $script);
          }
        }
      }
      else {
        return $output[$function_type][$function_name];
      }
    }
    else {
      return $output[$function_type];
    }
  }
  else {
    return $output;
  }
}

function we_javascript_output() {
  $output_array = array();

  foreach (we_javascript_output_array() as $function_type => $content) {

    // Special embed for class and functions
    switch ($function_type) {
      case 'string' :
        $output_array[$function_type] = json_encode($content);
        break;

      case 'css' :
        $output_array[$function_type] = json_encode(we_javascript_output_array($function_type));
        break;

      case 'class' :
      case 'function' :
      case 'jquery' :
      case 'inline' :
        $output_array[$function_type] = '{' . implode(',', array_reverse(we_javascript_output_array($function_type))) . '}';
        break;

      // uses "all" as key name
      case 'image' :
      case 'jquery_caller' :
      case 'script_url' :
        $output_array[$function_type] = json_encode(we_javascript_output_array($function_type, 'all'));
        break;
    }
  }

  $output = array();
  foreach ($output_array as $type => $content) {
    $output[] = '_' . $type . ':' . $content . '';
  }

  $output_json = trim(implode(',', $output));

  if ($output_json != '') {
    // If packed we need extra brackets
    //$output_json = (variable_get('we_javascript_pack',0)) ? '({(function(){ return {' . $output_json . '}; })();});' : '{' . $output_json . '}';
    $output_json = (variable_get('we_javascript_pack', 0)) ? '(function(){ return {' . $output_json . '}; })();' : '{' . $output_json . '}';
    return we_javascript_pack($output_json);
  }

  return FALSE;
}

/**
 * Pack javascript code
 */
function we_javascript_pack($script, $forced = FALSE) {

  if ($forced == TRUE || variable_get('we_javascript_pack', 0)) {
    $libs = libraries_get_libraries();
    $packer_url = $libs['JavaScriptPacker'] . '/JavaScriptPacker.inc';
    if ($libs['JavaScriptPacker'] && is_file($packer_url)) {
      require_once $packer_url;
      $packer = new JavaScriptPacker($script, 'Normal', FALSE, FALSE);
      return $packer->pack();
    }
    else {
      drupal_set_message('You must install the JavaScriptPack library if you want to use javascript files compression.', 'error');
    }
  }

  return $script;
}

/**
 * Combine an absolute path with a relative one.
 * Used to replace css url('...'); path with an absolute path
 * before transmitting it to json.
 *
 * Code found at : http://php.net/manual/fr/function.realpath.php
 * By Isaac Z. Schlueter
 */
function we_javascript_resolve_path($base, $href) {

  // href="" ==> current url.
  if (!$href) {
    return $base;
  }

  // href="http://..." ==> href isn't relative
  $rel_parsed = parse_url($href);
  if (array_key_exists('scheme', $rel_parsed)) {
    return $href;
  }

  // add an extra character so that, if it ends in a /, we don't lose the last piece.
  $base_parsed = parse_url("$base ");
  // if it's just server.com and no path, then put a / there.
  if (!array_key_exists('path', $base_parsed)) {
    $base_parsed = parse_url("$base/ ");
  }

  // href="/ ==> throw away current path.
  if ($href{0} === "/") {
    $path = $href;
  }
  else {
    $path = dirname($base_parsed['path']) . "/$href";
  }

  // bla/./bloo ==> bla/bloo
  $path = preg_replace('~/\./~', '/', $path);

  // resolve /../
  // loop through all the parts, popping whenever there's a .., pushing otherwise.
  $parts = array();
  foreach (
  explode('/', preg_replace('~/+~', '/', $path)) as $part
  )
    if ($part === "..") {
      array_pop($parts);
    }
    elseif ($part != "") {
      $parts[] = $part;
    }

  return (
          (array_key_exists('scheme', $base_parsed)) ?
                  $base_parsed['scheme'] . '://' . $base_parsed['host'] : ""
          ) . "/" . implode("/", $parts);
}

/**
 * Return javascript code to use at bottom of AJAX HTML returned content.
 * It will reload all javascripts used into the returned HTML datas
 * like a normal loading on page startup.
 */
function we_javascript_footer_ajax() {

  foreach (we_javascript_output_array() as $type => $scripts) {
    we_javascript_add_jquery('add_css_link');
  }

  $js = 'if ($we && $we.started==true){ ' . we_javascript_footer_unpacker() . ' } ';
  return ($js) ? '<script type="text/javascript"> ' . $js . ' </script>' : NULL;
}

/**
 * Return we_javascript bootstrap
 */
function we_javascript_footer_unpacker() {

  // Load functions and class called for this page
  $output = we_javascript_output();

  // Add json parsing on page startup
  if ($output) {

    // Unpack startup package
    $js .= '$we.unpack(' . $output . ',function(){';
    // Execute startup functions
    $js .= '$we.ready_complete(); ';

    $js .= '});';

    // Return script tag
    return $js;
  }
}
