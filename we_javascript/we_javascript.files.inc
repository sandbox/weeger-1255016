<?php

/**
 * @file
 * Files and directories scan managment functions
 */

/**
 * Scan whole module directory and return scripts files.
 * This function can be used as a shortcut if the list of scripts
 * is not defined manualy into the module.
 */
function we_javascript_scan_module($module_name) {
  $output = array();

  foreach (we_javascript_files_types() as $type) {
    $output[$type] = we_register_scan_module($module_name, $type, array(
        'extensions' => array('css', 'js')
            ));
  }

  return $output;
}

/**
 * Get list of scripts
 */
function we_javascript_get_scripts() {
  static $scripts = array();

  $scripts = ($forced) ? array() : $scripts;

  if (empty($scripts)) {

    // Scan modules directories
    foreach (module_implements('we_javascript_scripts') as $module) {
      $output = module_invoke($module, 'we_javascript_scripts');
      foreach (we_javascript_files_types() as $type) {
        if (isset($output[$type])) {
          $scripts[$type] = (is_array($scripts[$type])) ? $scripts[$type] : array();
          $scripts[$type] += $output[$type];
        }
      }
    }
  }

  return $scripts;
}