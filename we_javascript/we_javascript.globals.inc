<?php
/**
 * @file
 * Global variables used into we_javascript processing
 */

/**
 * Return files types. Used into AJAX request menu path
 * and for files prefixes.
 */
function we_javascript_files_types() {
  return array('function', 'class', 'jquery', 'inline', 'css');
}

/**
 * Return list of data that can be loaded by WE.
 * If a function is present to load datas in javascript, the equivalent function
 * have to be in PHP to load the same content on page startup.
 * javascript ->	$we.load_xxx();
 * php 		  -> 	we_javascript_add_xxx();
 */
function we_javascript_js_load_types() {
  return array('function', 'class', 'jquery', 'js', 'css');
}

function we_javascript_js_call_types() {
  return array('function', 'inline');
}

/**
 * Set / Get value of current request type. Used in first case to
 * define if the request is via AJAX or not, and return good content :
 * AJAX -> return JSON
 * NOT AJAX -> return Javascript embeded into template
 */
function we_javascript_ajax_request($value = FALSE) {
  static $we_javascript_ajax_request = NULL;

  if ($value != NULL && $we_javascript_ajax_request == NULL) {
    $we_javascript_ajax_request = $value;
  }

  return $we_javascript_ajax_request;
}

/**
 * Return the list of path used for ajax calls.
 * Used to
 *   - define AJAX menus items in hook_menu
 *   - detect if current page is AJAX or not.
 */
function we_javascript_menu_ajax() {
  $paths = array();

  foreach (we_javascript_files_types() as $type) {
    $paths['we_javascript/' . $type . '/%'] = 'we_javascript/' . $type . '/%';
  }

  return $paths;
}