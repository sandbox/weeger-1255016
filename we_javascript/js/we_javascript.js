/* WE Javascript | Core of we_javascript
 * Romain WEEGER 2010 / 2011 */

var $we = {};
 
/**
 * Only one instance of this object is created per page.
 */
we_javascript_class = $.inherit({
	
  __constructor:function() {
    this.ready_functions = [];
    this.started = false;
    this.loaded = {
      _string:{},
      _function:{},
      _class:{},
      _inline:{},
      _jquery:{}
    };

    this.loading_process_counter = 0; 	// Used to attribut an unique id for each loading process

    // Create a style container used when css are loaded
    $('head').append('<style id="we_javascript-styles"></style>');
    this.$_styles = $('#we_javascript-styles');
  },

  /**
   * Add base URL and "?q=" if needed
   * Used for Drupal to keep cleans URLs
   */
  url:function(url,settings) {

    // Suport non clean urls
    // Add index.php to work with non Apache servers
    if (Drupal.settings.we_javascript.clean_url!=1 && (settings==undefined || !('file' in settings) || settings.file!=true)) {
      return 'index.php?q=' + url;
    }

    if (settings!=undefined && 'absolute' in settings && settings.absolute==true) {
       return Drupal.settings.we_javascript.base_url + '/' + url;
    }

    return Drupal.settings.basePath + url;
    
  },

  /**
   * Returns path to module
   */
  url_module:function(module_name,settings) {
    // options 'file' must be to true to remove 'q?=' into non clean urls
    var settings = (settings==undefined) ? {} : settings;
    return this.url(Drupal.settings.we_javascript.modules[module_name].path,$.extend(settings,{file:true}));
  },

  /**
   * Returns path to theme
   */
  url_to_theme:function(settings) {
    // options 'file' must be to true to remove 'q?=' into non clean urls
    var settings = (settings==undefined) ? {} : settings;
    return this.url(Drupal.settings.we_javascript.path_to_theme,$.extend(settings,{file:true}));
  },

  //__________________________________________________ C A L L E R S

  _call:function(type,name,arguments,complete) {
    var function_name = 'call_' + type;
    if (function_name in this && typeof(this[function_name])=='function') {
      this[function_name](name,arguments,complete);
    }
  },

  /**
   * Load a function if not exists, and execute it after.
   */
  call_function:function(name,arguments,complete) {

    var callback_name = name;
    var callback_arguments = arguments;
    var callback_complete = complete;
    var callback_this = this;

    this.load_function(name,function() {

      // Prevent recursion if not loaded
      if (callback_name in $we.loaded['_function']) {

        // Create the javascript call for eval like this : 
        // name(arguments[0],arguments[1],...);
        var args_code_array = new Array();
        for (var arg in callback_arguments) {
          args_code_array[arg] = "callback_arguments[" + arg + "]";
        }

        var args_code = args_code_array.join(",");
        var eval_content = '$we.loaded["_function"].' + callback_name + '(' + args_code + ');';
        output = eval(eval_content);

      }
      else {
        alert('Unable to load function : ' + callback_name);
      }

      if (callback_complete!=null) { 
        callback_complete(output);
      }
    });
  },

  call_inline:function(name,arguments,complete) {

    var callback_name = name;
    var callback_complete = complete;
    var callback_this = this;

    var complete = function() {
      // Prevent recursion if not loaded
      if (!(callback_name in callback_this.loaded['_inline'])) {
        alert('Unable to load inline : ' + callback_name);
      }

      if (callback_complete!=null) { 
        callback_complete(output);
      }
    };

    if (name in this.loaded['_inline']) {
      complete();
    }
    else {
      this._load_script('inline',name,complete);
    }
  },

  //__________________________________________________ A J A X   L O A D E R S

  // Loader function used when script shoudn't be parsed by php
  _load:function(type,name,complete) {
    var function_name = 'load_' + type;
    if (function_name in this && typeof(this[function_name])=='function') {
      this[function_name](name,complete);
    }
  },

  load_function:function(name,complete) {
    if (!(name in this.loaded['_function'])) {
      this._load_script('function',name,complete);
    }
    else if (complete!=null) {
      complete();
    }
  },

  load_class:function(name,complete) {
    if (!(name in this.loaded['_class'])) {
      this._load_script('class',name,complete);
    }
    else if (complete!=null){
      complete();
    }
  },

  /**
   * This function is basically used in internal to handle
   * calls to jquery plugins.
   * If you want to load a plugin replace
   * $we.load_jquery('my_plugin'); by the classic way : $('selector').my_plugin();
   */ 
  load_jquery:function(name,complete) { 
    if (!(name in this.loaded['_jquery']) || this.loaded['_jquery'][name]==undefined) {        
      this._load_script('jquery',name,complete);
    }
  },

  /**
   * CSS files are parsed by WE Javascript and loaded as a json object.
   * Image files are loaded by javascript and styles declaration are append
   * to a specific <style> tag at the top of the document.
   * CSS content is append only when all contained images are completly loaded.
   */
  load_css:function(name,complete) {
    if (!(name in this.loaded['_css'])) {
      this._load_script('css',name,complete);
    }
    else if (complete!=null){
      complete();
    }
  },

  /**
   * Image doesn't pass trough PHP. This is a
   * classic javascript call.
   */
  load_image:function(url,complete) {
    new we_javascript_loading_process({
      scripts:[{
        type:'image',
        url:url
      }],
      complete:complete
    });
  },

  /**
   * Helper.
   */
  _load_script:function(script_type,script_name,complete) {
    this._load_script_multiple({
      scripts:[{
        name: script_name,
        type: script_type
      }],
      complete:complete
    });
  },

  _load_script_multiple:function(options) {
    // Create loading process
    new we_javascript_loading_process(options);
  },

  //__________________________________________________ M I S C   L O A D E R S

  /**
   * Parse json data.
   * Used when datas is not loaded by AJAX.
   * Basicaly as document startup
   */
  unpack:function(data,complete) {
    this._load_script_multiple({
      scripts:[{
        type:'json',
        data:data
      }],
      complete:complete
    });
  },

  /**
   * Because we javascript must execute loading request on startup,
   * he needs his own ready function.
   * We may merge this function with the jQuery ready function if we
   * found a way to contruct the $we object before.
   */
  ready:function(callback) {
    this.ready_functions.push(callback);
  },

  /**
   * Execute all "ready" functions
   */
  ready_complete:function() {
    for (var i in this.ready_functions) {
      this.ready_functions[i]();
    }
    this.started = true;
  },

  /**
   * Helper function, used by jQuery plugin as a default laucher function.
   */
  _jquery_loader_function:function(callback_this,callback_caller,callback_options) {

    // Prevent recursion if loading failed
    if ($we.loaded['_jquery'][callback_caller]==undefined) {
      $we.load_jquery(callback_caller,function() {
        callback_this[callback_caller](callback_options);
      });
      $we.loaded['_jquery'][callback_caller] = true;
    }
    return callback_this.each(function(){});
  }
	
});

/**
 * Loading process.
 * we_javascript allow to load multiple loading process
 * Each process can load a script or a collection of differents scripts.
 * TODO : Each process can be totally completed asynchronously,
 * frequently delayed when preloading images files used by css
 * So each loading can execute a "complete" callback when finished.
 */
we_javascript_loading_process = $.inherit({
	
  __constructor:function(options) {
    $.extend(this,options);
    this.loading_queue = {};
    this.loading_queue_counter = 0;
    this.started = false;
    this.styles = [];		// String storing css datas before append it to $_styles on complete
    this.complete_arguments = new Array();
                
    this.loading_start();
		
    for (var i in this.scripts) {
      switch (this.scripts[i].type) {
        case 'function' : 
        case 'class' :
        case 'jquery' :
        case 'inline' :
        case 'css' :
					
          // Load remote scripts
          var callback_this = this;
          var loading_queue_id = this.loading_queue_append();

          this.get(this.scripts[i].type,this.scripts[i].name,function (data) {
            if (data) {
              data = eval("(" + data + ")");
              callback_this.parse(data);
              // loading_complete called if loading_queue_complete is the last one.
              callback_this.loading_queue_complete(loading_queue_id);
            }
          });
					
          break;
					
        case 'json' :
          this.parse(this.scripts[i].data);
          this.loading_complete();
          break;
					
        case 'image' :
          this.parse({
            '_image':{
              0:this.scripts[i].url
              }
            });
        // loading_complete called when completely loaded
        break;
				
      }
    }
  },
	
  get:function(script_type,script_name,callback) {
    var callback_this = this;
    var url = $we.url('we_javascript/' + script_type + '/' + script_name);

    $.ajax({
      url:url,
      async:false,
      success:callback
    });
  },
	
  /**
   * Parse recevied json
   */
  parse:function(data) {

    // jQuery
    if ('_jquery_caller' in data) {
      for (var i in data['_jquery_caller']) { 
        // Verify if plugin is not allready loaded
        if ($we.loaded['_jquery'][data['_jquery_caller'][i]]!=true && !(data['_jquery_caller'][i] in jQuery.fn)) {
          // Create an empty jQuery plugin
          // Just to handle call, and query by ajax
          jQuery.fn[data['_jquery_caller'][i]] = (function (caller) {
            var callback_caller = caller;
            return function(options) {
              // Use a common function
              return $we._jquery_loader_function(this,callback_caller,options);
            };
          })(data['_jquery_caller'][i]);
        }
      }
    }
    
    if ('_string' in data) {
      for (var i in data['_string']) {
        $we.loaded['_string'][i] = data['_string'][i];
      }
    }
    
    // Add to this.styles, added in head at loading end
    if ('_css' in data) { 
      for (var i in data['_css']) {
        if (!(i in $we.loaded['_css'])) {
          this.styles[i] = $we.url(data['_css'][i]);
          $we.loaded['_css'][i] = true; 
        }
      }
    }
	
    if ('_image' in data) {
      for (var i in data['_image']) {
				
        var img = new Image;
        var callback_this = this;
				
        img.loading_id = this.loading_queue_append();
        img.onload = function() {
          callback_this.loading_queue_complete(this.loading_id,[img]);
        };
				
        img.src = data['_image'][i];
				
      }
    }
    
    if ('_class' in data) { 
      for (var i in data['_class']) {
        $we.loaded['_class'][i] = data['_class'][i];
        var eval_text = i + ' = data[\'_class\'][i]();';
        eval(eval_text);
      }
    }
		
    if ('_jquery' in data) { 
      for (var i in data['_jquery']) {
        data['_jquery'][i]();
        $we.loaded['_jquery'][i] = true;
      }
    }
		
    if ('_function' in data) {
      for (var i in data['_function']) {
        if (typeof($we.loaded['_function'][i])!="function"){
          $we.loaded['_function'][i] = data['_function'][i]();
          var eval_text = i + ' = $we.loaded[\'_function\'][i];';
          eval(eval_text);
        }
      }
    }
		
    if ('_inline' in data) { 
      for (var i in data['_inline']) {
        $we.loaded['_inline'][i] = data['_inline'][i];
        $we.loaded['_inline'][i]();
      }
    }
  },
	
  /**
   * Add step to complete by this loading process
   */
  loading_queue_append:function() {

    var id = 0;
    for (var i in this.loading_queue) {
      id++;
    }
		
    this.loading_queue[id] = true;
		
    return id;
  },
	
  loading_queue_complete:function(id,complete_arguments) {
    delete this.loading_queue[id];

    for (var i in this.loading_queue) {
      return false;
    }
    
    this.loading_complete(complete_arguments);
  },
	
  loading_start:function() {
    $('body').trigger('we_javascript.loading_process_start');
  },
	
  loading_complete:function(complete_arguments) {

    // Add styles to head
    for (var i in this.styles) {
      jQuery(document).add_css_link(this.styles[i]);
    }

    // Execute complete callback
    // Pass complete arguments
    if (typeof(this.complete)=='function') {
      
      var args_code_array = new Array();
      for (var arg in complete_arguments) {
        args_code_array[arg] = "complete_arguments[" + arg + "]";
      }

      var args_code = args_code_array.join(",");
      var eval_content = 'this.complete(' + args_code + ');';
      eval(eval_content);

    }

    // Event
    $('body').trigger('we_javascript.loading_process_complete');
  }
});

/**
 * Create $we object when document is ready
 */
$(function() {
  $we = new we_javascript_class();
});
