(function($){
	$.fn.add_js_link = function(url) {
		
		// Create an object to save loaded urls
		if (!('loaded_js_link' in $)) {
			$.loaded_js_link = {};
		}
		
		if (!(url in $.loaded_js_link)) {
			// Add code
			$('head').append('<script id="we_javascript_new_js_link" type="text/javascript" src="' + url + '"></script>');
			// Save reference
			$.loaded_js_link[url] = $('#we_javascript_new_js_link');
			// Remove id attribute
			$.loaded_js_link[url].removeAttr('id');
		}
		
		// Return nothing
		return this.each(function(){});
	};
})(jQuery);