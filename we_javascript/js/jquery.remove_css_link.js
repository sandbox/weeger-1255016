(function($){
	$.fn.remove_css_link = function(url) {
		
		if (url in $.loaded_css_link) {
			$.loaded_css_link[url].remove();
			delete $.loaded_css_link[url];
		}
		
		// Return nothing
		return this.each(function(){});
	};
})(jQuery);