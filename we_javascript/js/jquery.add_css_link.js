(function($){
	$.fn.add_css_link = function(url) {
		
		// Create an object to save loaded urls
		if (!('loaded_css_link' in $)) {
			$.loaded_css_link = {};
		}
		
		if (!(url in $.loaded_css_link)) {
			// Add code
			$('head').append('<link id="we_javascript_new_css_link" type="text/css" rel="stylesheet" media="all" href="' + url + '"/>');
			// Save reference
			$.loaded_css_link[url] = $('#we_javascript_new_css_link');
			// Remove id attribute
			$.loaded_css_link[url].removeAttr('id');
		}
		
		// Return nothing
		return this.each(function(){});
	};
})(jQuery);