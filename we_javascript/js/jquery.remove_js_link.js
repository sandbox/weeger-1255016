(function($){
	$.fn.remove_js_link = function(url) {
		
		if (url in $.loaded_js_link) {
			$.loaded_js_link[url].remove();
			delete $.loaded_js_link[url];
		}
		
		// Return nothing
		return this.each(function(){});
	};
})(jQuery);