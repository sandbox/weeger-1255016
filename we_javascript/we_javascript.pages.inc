<?php
/**
 * @file
 * Various pages used into we_javascript process and configuration
 */

/**
 * AJAX Page.
 * Return called function or class.
 */
function _page_we_javascript_load_script($function_name, $function_type) {

  // Save AJAX request as TRUE
  we_javascript_ajax_request(TRUE);

  // Init modules
  module_invoke_all('we_javascript_init_ajax');

  _we_javascript_add($function_type, $function_name);

  // Display JSON
  print we_javascript_output();
}

/**
 * Settings form
 */
function _form_we_javascript_admin_settings() {
  $form = array();

  $form['we_javascript_pack'] = array(
      '#type' => 'checkbox',
      '#title' => t('Compress JavaScript files'),
      '#default_value' => variable_get('we_javascript_pack', 0),
      '#description' => t('If checked, Javascript files used by WE Javascript will be packed. It can be create some troubles to identify bugs on debugging.'),
  );

  return system_settings_form($form);
}
