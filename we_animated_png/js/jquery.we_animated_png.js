/*
 * WE Animated PNG for Drupal 6.x
 * Romain WEEGER 2010 / 2011
 * --------------------
 * Provides tools to fake PNG animation, used to have
 * Images in full colors and alpha colors animated
 */

(function($){
	
	Drupal.settings.we_animated_png = {
		counter:0,
		png_list:{},
		png_list_$:{}
	};

	$.fn.we_animated_png = function(options) {

		options = (typeof(options)=='string') ? {url:options} : options;
		
		var defaults = {
			destination:null,
			url:null,
			width:64,
			height:73,
			frames:51,
			framerate:25,
			frame_start:0,
		};
		
		options = $.extend(defaults,options);
		
		var callback_this = this;
		var callback_options = options;
		
		$we.load_class('we_animated_png',function() {
			callback_this.each(function(){
				callback_options.destination = $(this);
				var png = new we_animated_png(callback_options);
				if ('autostart' && callback_options && callback_options.autostart==true) {
					png.start();
				}
			});
		});

		return this.each(function(){});
	};
})(jQuery);