/*
 * WE Animated PNG for Drupal 6.x
 * Romain WEEGER 2010 / 2011
 * --------------------
 * Provides tools to fake PNG animation, used to have
 * Images in full colors and alpha colors animated
 */

return $.inherit({
	__constructor:function(datas) {
		
		this.index = Drupal.settings.we_animated_png.counter++;
		this.destination = false;
		this.url = false;
		this.width = 0;
		this.height = 0;
		this.frames = 0;
		this.framerate = 25;
		this.interval = false;
		this.frame_start = 0;
		this.frame_temp = 0;
		this.frame = 0;
		this.repeat = true;
		this.direction = 1;
		this.orientation = 'x';
		this.loop = true;
		
		$.extend(this,datas);
		
		/*if (this.direction<0) {
			if (!('frame_start' in this)) {
				this.frame = this.frames-1;
			}
		}*/
		
		this.frame = this.frame_start;
		this.offset = (this.orientation=='x') ? this.width : this.height;
		this.repeat = (this.repeat==false) ? 1 : this.repeat;
		this.elm = this.destination.get(0);
		Drupal.settings.we_animated_png.png_list[this.index] = this;
		Drupal.settings.we_animated_png.png_list_$[this.index] = this.destination;
		
		this.destination
		  .width(this.width)
		  .height(this.height)
		  .css('background-image',"url('" + this.url + "')");
		
		$we.load_image(this.url);
		
		this.reset();
		this.goto_frame(this.frame_start);
		
	},

	start:function(options) {
		
		if (this.destination!=false && this.url!=false && this.frames>0) {
			
			if (this.interval!=false) {
				this.stop();
			}
			
			$.extend(this,options);
			
			this.position_temp = 0;
			this.sign = (this.direction<0) ? -1 : 1;
			this.repeated = 0;
			
			// Execute first image
			this.frame_interval();
			
			// Start interval
			var callback_this = this;
			this.interval = setInterval(function(){ 
				callback_this.frame_interval();
			},1000 / this.framerate);

		}
	},
	
	stop:function() {
		window.clearInterval(this.interval);
		this.interval = false;
	},
	
	frame_interval:function(){ 
		
		// Use frame + direction in place of frame++, to allow to control speed
		this.frame_next = (this.frame + this.direction);
		this.frame_temp++;
		
		// Reset frame_next to 0 on loop
		if ((this.direction>0 && this.frame_next>this.frames)
			||
			(this.direction<0 && this.frame_next<-(this.frames))) {
			this.frame_next = 0;
		}

		if (this.frame_temp > this.frames) {
			this.repeated++; 
			this.frame_temp = 0;
		}
		
		// Count repeats
		if ((this.repeat!==true && !isNaN(this.repeat) && this.repeated >= this.repeat)
				||
			(this.loop==false && (this.frame_next<0 || this.frame_next>=this.frames))){
			this.stop();
			return false;
		}				
		
		this.goto_frame(this.frame_next);
    
	},
	
	goto_frame:function(frame) {
		this.frame = frame;
		this.position_temp = -(this.frame * this.offset);
		this.elm.style.backgroundPosition = (this.orientation=='x') ? this.position_temp + 'px 0' : '0 ' + this.position_temp + 'px';
		
	},
	
	reset:function(options) {
		this.frame = 0;
		$.extend(this,options);
	}
});