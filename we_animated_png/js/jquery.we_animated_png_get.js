/*
 * WE Animated PNG for Drupal 6.x
 * Romain WEEGER 2010 / 2011
 * --------------------
 * Provides tools to fake PNG animation, used to have
 * Images in full colors and alpha colors animated
 */

(function($){
	// Return we_anumated_png object for given jQuery selector
	$.fn.we_animated_png_get = function(options) {
		options = (typeof(options)=='function') ? {success:options} : options;
		
		return this.each(function(){
			if (Drupal.settings.we_animated_png!=undefined) {
				var elm = $(this).get(0); 
				for (var i in Drupal.settings.we_animated_png.png_list) { 
					Drupal.settings.we_animated_png.png_list_$[i].css('margin-top','1px solid red');
					if (Drupal.settings.we_animated_png.png_list_$[i].get(0)===elm) {
						
						if (options.success!=undefined) {
							// Return png, not element
							return options.success(Drupal.settings.we_animated_png.png_list[i]);
						}
					}
				}
			}
			return false;
		});
	};
})(jQuery);