/*
 * WE Animated PNG for Drupal 6.x
 * Romain WEEGER 2010 / 2011
 * --------------------
 * Provides tools to fake PNG animation, used to have
 * Images in full colors and alpha colors animated
 */

(function($){
	// Reset
	$.fn.we_animated_png_reset = function(options) {
		return this.each(function(){
			$(this).we_animated_png_get(function(png){
				png.reset(options);
			});
		});
	};
})(jQuery);