/*
 * WE Animated PNG for Drupal 6.x
 * Romain WEEGER 2010 / 2011
 * --------------------
 * Provides tools to fake PNG animation, used to have
 * Images in full colors and alpha colors animated
 */

(function($){
	// Start
	$.fn.we_animated_png_start = function(options) {
		return this.each(function(){
			$(this).we_animated_png_get(function(png){
				png.start(options);
			});
		});
	};
})(jQuery);