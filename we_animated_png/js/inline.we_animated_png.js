$(document).ready(function() {
	$we.ready(function() {
		$('#content img[rel^="we_animated_png"]').each(function() {
			var rel = $(this).attr('rel');
			var id = $(this).attr('id');
			rel = rel.split(';');
			options = new Array;
			
			for (var i in rel) {
				var exp = rel[i].split('=');

				if (exp[1]=='true') {
					exp[1] = true;
				}
				else if (new Number(exp[1])==exp[1]) {
					exp[1] = new Number(exp[1]);
				}
				options[exp[0]] = exp[1];
			}
			
			options.url = $(this).attr('src');
			$(this).replaceWith('<div id="we_animated_png-handler"></div>');
			$('#we_animated_png-handler').attr('id',id).we_animated_png(options);
		});
	})
});