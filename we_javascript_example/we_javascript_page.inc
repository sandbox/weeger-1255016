<?php

/**
 * @file
 * Describe test page HTML content.
 */

/**
 * Display content of testing page
 */
function _page_we_javascript_example() {
  $output = '';

  $output .= '<p>';
  $output .= 'WE Javascript is a JS ressources register. It index all javascript (and some specificaly named css) files and offer to load it by AJAX when function is called. It optionnaly can compress JS files in the same time using the Dean Edwards JavaScript\'s Packer.';
  $output .= '</p>';

  $output .= '<p>';
  $output .= 'This page is used to test several behaviors of WE Javascript (beta version) for  ' . l('Drupal 6.x', 'http://www.drupal.org') . '. First, sorry if everything is not clear in this documentation, or if my english is not pretty good.';
  $output .= 'Hope this module will be usefull. To understand the interest of these tests, you can process like that : ';
  $output .= theme('item_list', array(
      'Click on "Show source"',
      'Click on "Execute"',
      'Click on "Show source" again',
          ), NULL, 'ol');
  $output .= '<br/>If test succeed, you will see a javascript alert box, then, the source of the tested object will change, replaced by the requested function definition. ';
  $output .= 'See into your debug console the AJAX content loaded by WE Javascript.';

  $output .= theme('fieldset', array(
      '#title' => 'HOWTO : Create a Drupal module using WE Javascript',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => theme('item_list', array(
          'Create / enable your custom module',
          'Add this hook to detect js files : '
          . '<br/><code>/**'
          . '<br/>&nbsp;*&nbsp;Implementation of hook_we_javascript_scripts()'
          . '<br/>&nbsp;*/'
          . '<br/>function mymodule_we_javascript_scripts() {'
          . '<br/>&nbsp;&nbsp;&nbsp;&nbsp;return we_javascript_scan_module(\'mymodule\');'
          . '<br/>}</code>',
          'Follow HOWTOs for witch kind of script you need'
              ), NULL, 'ol'),
          ));
  $output .= '</p>';

  // _____ Javascript functions _____ //
  $output .= '<h2>Javascript functions</h2>';
  $output .= '<p>';
  $output .= 'Functions loaded with WE uses global namespace for functions. After first loading, if you are sure function exists, you can load it directly with <b>my_function();</b>';
  $output .= '</p>';
  $output .= '<input type="button" value="Show source" onclick="if (typeof(we_javascript_example_function)!=\'undefined\') { alert(we_javascript_example_function); } else { alert(\'we_javascript_example_function() is not defined.. Try to call_function ? \'); }" />';
  $output .= '<input type="button" value="$we.call_function(\'we_javascript_example_function\');" onclick="$we.call_function(\'we_javascript_example_function\');" />';

  $output .= '<p>';
  $output .= theme('item_list', array(
      'Javascript : <b>$we.load_function("function_name");</b> to load function.',
      'Javascript : <b>$we.call_function("function_name",["arg1","arg2"]);</b> to execute it in the same time.',
      'PHP : <b>we_javascript_add_function("function_name");</b> to add function to the rendered page.',
          ), NULL, 'ul');
  $output .= '</p>';

  $output .= theme('fieldset', array(
      '#title' => 'HOWTO : Add a function to my module',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => theme('item_list', array(
          'Create a Drupal module using WE Javascript (see previous HOWTO)',
          'Add your function in a file and name it like : <b>function.my_function.js</b>',
          'Your function definition must look like <b>return function() { ... } </b>. This is an anonymous function.',
          'Call your function like this $we.call_function("my_function",["arg1","arg2",...]);'
              ), NULL, 'ol'),
          ));

  // _____ Javascript class objects _____ //
  $output .= '<h2>Javascript class objects</h2>';
  $output .= 'The button below will execute this test code : ';
  $output .= '<pre><code id="we_javascript_class_code">$we.load_class(\'we_javascript_example_class\'); ' . "\n" . 'var obj = new we_javascript_example_class(); ' . "\n" . 'obj.test();</code></pre>';
  $output .= '<input type="button" value="Show source" onclick="if (typeof(we_javascript_example_class)!=\'undefined\') { alert(we_javascript_example_class); } else { alert(\'we_javascript_example_class is not defined.. Try to load_class ? \'); }" />';
  $output .= '<input type="button" value="Execute" onclick="eval($(\'#we_javascript_class_code\').html());" />';

  $output .= '<p>';
  $output .= theme('item_list', array(
      'Javascript : <b>$we.load_class("class_name");</b> to load class.',
      'PHP : <b>we_javascript_add_class("class_name");</b> to add class to the rendered page.',
          ), NULL, 'ul');
  $output .= '</p>';

  $output .= theme('fieldset', array(
      '#title' => 'HOWTO : Add a javascript class to my module',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => theme('item_list', array(
          'Create a Drupal module using WE Javascript (see previous HOWTO)',
          'Add your class in a file and name it like : <b>class.my_class.js</b>',
          'Your class definition must look like <b>return { ... class object prototype ... } </b>.',
          'Load your class like this $we.load_class("my_class");',
          'Play with it : obj = new my_class(); ',
              ), NULL, 'ol'),
          ));

  // _____ jQuery plugin _____ //
  $output .= '<h2>jQuery plugin</h2>';
  $output .= '<p>';
  $output .= 'We have created a standard jquery plugin called <b>example (jquery.example.js)</b>, we can use it like that : <b>$(document).example();</b>.';
  $output .= '</p>';
  $output .= '<div><input type="button" value="Show source" onclick="alert($(document).example);" />';
  $output .= '<input type="button" value="Execute $(document).example(\'arg\',\'arg2\');" onclick="$(document).example(\'arg\',\'arg2\');" /></div>';

  $output .= '<p>';
  $output .= theme('item_list', array(
      'Javascript : <b>$("selector").plugin_name("args");</b> to load plugin and execute it.',
      'PHP : <b>we_javascript_add_jquery("plugin_name");</b> to add plugin to the rendered page.',
          ), NULL, 'ul');
  $output .= '</p>';

  $output .= theme('fieldset', array(
      '#title' => 'HOWTO : Add a jQuery plugin to my module',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => theme('item_list', array(
          'Create a Drupal module using WE Javascript (see previous HOWTO)',
          'Add your jQuery plugin file and name it like : <b>jquery.myplugin.js</b>',
          'Have fun : just call your plugin when needed $("selector").myplugin("arg");',
              ), NULL, 'ol'),
          ));

  // _____ Inline javascript _____ //
  $output .= '<h2>Inline javascript</h2>';

  $output .= '<p>';
  $output .= 'You can add inline javascript by several ways in PHP.';
  $output .= theme('item_list', array(
      '<b>we_javascript_add_inline("script_name");</b> with "script_name" the name of the <b>inline.script_name.js</b>',
      '<b>we_javascript_add_inline("script/local/path.js");</b> width path of the inline.script_name.js',
      '<b>we_javascript_add_inline("name",alert(\"My script !\");");</b> with your script directly passed to the function.',
          ), NULL, 'ul');
  $output .= '</p>';

  $output .= 'In javascript you can only load an execute javascript name like <b>inline.my_script.js</b>. ';
  $output .= 'You can\'t load complete javascript path via AJAX for security reasons. ';

  $output .= '<p>';
  $output .= theme('item_list', array(
      '<b>$we.load_inline("name");</b> to load named javascript file. (removed)',
      '<b>$we.call_inline("name");</b> to execute it in the same time.',
          ), NULL, 'ul');
  $output .= '</p>';

  $output .= '<input type="button" value="$we.call_inline(\'we_javascript_example_inline\');" onclick="$we.call_inline(\'we_javascript_example_inline\');" />';

  $output .= '<p>';
  $output .= 'If you want to load remote JS files like &lt;script src="yourfile.js"&gt;&lt;/script&gt; see <b>add_js_link</b> jQuery plugin into <b>WE Javascript tools</b> module.';
  $output .= '</p>';

  $output .= '<h2>CSS files</h2>';
  $output .= '<p>';
  $output .= 'CSS files are parsed by WE Javascript and loaded as a json object. Image files are loaded by javascript and styles declaration are append to a specific &lt;style&gt; tag at the top of the document. ';
  $output .= 'CSS content is append <b>only when all contained images are completly loaded</b>.';
  $output .= '</p>';

  $output .= '<input class="we_javascript_css_test" type="button" value="$we.load_css(\'we_javascript_example_css\');" onclick="$we.load_css(\'we_javascript_example_css\');" />';

  $output .= '<h2>Images</h2>';
  $output .= '<pre><code id="we_javascript_image_code">$we.load_image($we.url_module(\'we_javascript_example\') + \'/images/big_one.jpg\',function() {' . "\n" . '  alert(\'Image loaded,\nyou can use it now,\nin CSS or in a img tag.\');' . "\n" . '});</code></pre>';

  $output .= '<input type="button" value="Execute" onclick="eval($(\'#we_javascript_image_code\').html());" />';

  // _____ Cascading calls / Code parsing _____ //
  $output .= '<h2>Cascading calls / Code parsing</h2>';
  $output .= '<p>Most of the time, javascript functions are not called alone. Scripts can be a long imbrication of different calls. For this case, javascripts files are parsed before sending to AJAX, and all functions calls are included into response.</p>';
  $output .= '<input type="button" value="Execute $(document).example_2();" onclick="$(document).example_2();" />';
  $output .= '<input type="button" value="$we.call_function(\'we_javascript_example_function_2\');" onclick="$we.call_function(\'we_javascript_example_function_2\');" />';

  // _____ Add related Javascripts _____ //
  $output .= '<h2>Add related Javascripts</h2>';
  $output .= '<p>';
  $output .= 'If you want to trigger a script loading and add other script to the package (from an other module for example) you can use the hook_we_javascript_add_script($function_type,$function_name,$script)';
  $output .= '</p>';
  $output .= '<div id="we_javascript_example_related" >This div will change on click on the button but, normal but...</div>';
  $output .= '<input type="button" value="Execute $we.call_inline(\'we_javascript_example_related\');" onclick="$we.call_inline(\'we_javascript_example_related\');" />';

  // _____ Startup package _____ //
  $output .= '<h2>Startup package</h2>';
  $output .= '<p>';
  $output .= 'You can still use the API into Drupal to preload content at page loadin without AJAX call.';
  $output .= '<div id="we_javascript_example_css_preloaded">This CSS style have been loaded at page startup.</div>';
  $output .= 'See Drupal API section below for more info.';
  $output .= '</p>';

  // _____ Javascript compression _____ //
  $output .= '<h2>Javascript compression</h2>';
  $output .= '<p>';
  $output .= 'WE Javascript can compress automatically all javascripts before send it in AJAX. This option can be disabled into WE configuration.';
  $output .= '</p>';

  // _____ Javascript API _____ //
  $output .= '<h2>Javascript API</h2>';
  $output .= '<p>';
  $output .= 'Inheritance plugin was used for define $we object, in order to keep a similar approach of PHP class constructors. ';
  $output .= 'I choose to create a separate $we object instead using functions as a jQuery plugin. First to have a separate namespace, secondly beceause any of theses object\'s method can be used alone, without the main object, and without Drupal too. ';
  $output .= 'Yet we can imagine to make this object a part of Drupal javascript object. <b>$we object uses jQuery but has no relation (D\'ont be confused by the $ in $we)</b> ';
  $output .= 'Here is the list of the most usefull functions of this object : ';
  $output .= theme('item_list', array(
      'call_function',
      'call_inline',
      'load_function',
      'load_class',
      'load_jquery',
      //'load_inline',
      'load_css',
      'load_image'
          ), NULL, 'ul');
  $output .= '</p>';

  // _____ Drupal API _____ //
  $output .= '<h2>Drupal API</h2>';
  $output .= '<p>';
  $output .= 'Here is the list of hooks created by the we_javascript Drupal module.';

  $output .= theme('item_list', array(
      'hook_we_javascript_init_page()	 : Launched to init normal pages (not AJAX calls)',
      'hook_we_javascript_init_ajax()  : Launched to init AJAX pages',
      'hook_we_javascript_scripts()    : Launched to get all available scripts',
      'hook_we_javascript_add_script() : Launched when a new script is added to output array',
          ), NULL, 'ul');
  $output .= '</p>';

  $output .= 'All the loading functions present into $we object have equivalent into we_javascript module. You can use it to preload all these datas on page startup without add an AJAX call, or to add related data into the AJAX response.';
  $output .= theme('item_list', array(
      'we_javascript_add_function',
      'we_javascript_add_class',
      'we_javascript_add_jquery',
      'we_javascript_add_inline',
      'we_javascript_add_css',
      'we_javascript_add_image',
          ), NULL, 'ul');
  $output .= '</p>';

  // Devel
  if (module_exists('devel')) {

    // Output array
    $output .= theme('fieldset', array(
        '#title' => 'WE scripts list',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#value' => '<div class="fieldset-wrapper">WE Uses this list to reach called files. For each kind of javascript file, only one name can be used. ' . kprint_r(we_javascript_get_scripts(), TRUE) . '</div>',
            ));

    // Output array
    $output .= theme('fieldset', array(
        '#title' => 'WE output on page load',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#value' => '<div class="fieldset-wrapper"> Here is the datas presents in this page startup ' . kprint_r(we_javascript_output_array(), TRUE) . '</div>',
            ));
  }

  // _____ Misc _____ //
  /* $output .= '<h2>Misc</h2>';
    $output .= '<p>';
    $output .= 'Here is the list of hooks created by the we_javascript Drupal module.';
    $output .= '</p>'; */

  return $output;
}