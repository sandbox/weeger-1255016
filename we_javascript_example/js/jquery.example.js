/*
 * Romain WEEGER 2010 / 2011
 * This is an example of a basic jquery plugin structure
 * To use it in WE Javascript you should remove the "_" before the
 * name of this file, then you might use it with $('selector').example(); directly.
 */
(function($){
	$.fn.example = function(options) {
		alert('It works for jQuery plugin ! Argument : ' + options);
		return this.each(function(){});
	};
})(jQuery);