<?php
/**
 * @file
 * Pages used for we_register configuration
 */

/**
 * Settings form
 */
function _form_we_register_admin_settings() {
  $form = array();

  $form['we_register_cache_scan'] = array(
      '#type' => 'checkbox',
      '#title' => t('Cache modules directories scans'),
      '#default_value' => variable_get('we_register_cache_scan', 0),
      '#description' => t('If checked, scan result will be set in cache'),
  );

  return system_settings_form($form);
}