<?php

/**
 * @file
 * Functions used to scan modules directory
 */

/**
 * Scan all modules and themes
 */
function we_register_scan_all($prefix, $options) {
  $output = we_register_scan_modules($prefix, $options);
  $output += we_register_scan_themes($prefix, $options);

  return $output;
}

/**
 * Scan all modules in one time
 */
function we_register_scan_modules($prefix, $options) {
  static $output = NULL;

  if ($output === NULL) {
    $output = array();
    $modules = module_list();
    foreach ($modules as $module_name) {
      $output = array_merge_recursive(we_register_scan_module($module_name, $prefix, $options), $output);
    }
  }

  return $output;
}

/**
 * Scan all modules in one time
 */
function we_register_scan_themes($prefix, $options) {
  static $output = NULL;
  $themes = list_themes();
  if ($output === NULL) {
    $output = array();
    $themes = list_themes();
    foreach ($themes as $theme_name => $data) {
      $output = array_merge_recursive(we_register_scan_dir(drupal_get_path('theme', $theme_name) . '/', $prefix, $options), $output);
    }
  }

  return $output;
}

/**
 * Scan module directory and set result in cache if actvated.
 */
function we_register_scan_module($module_name, $prefix, $options) {

  // Get all scans results
  $scans = variable_get('we_register_scans', array());

  if (!module_exists($module_name)) {
    $scans[$module_name] = array();
  }

  // Return cached content
  // If module list does not exists, the module have never been scaned
  if (isset($scans[$module_name]) && (!isset($options['forced']) || $options['forced'] != TRUE) && variable_get('we_register_cache_scan', 0) == 1) {
    return $scans[$module_name];
  }

  $scans[$module_name] = we_register_scan_dir(drupal_get_path('module', $module_name) . '/', $prefix, $options);

  // Save
  if (variable_get('we_register_cache_scan', 0) == 1) {
    variable_set('we_register_scans', $scans);
  }

  return $scans[$module_name];
}

/**
 * Scan directory and return supported javascripts files
 */
function we_register_scan_dir($directory = FALSE, $prefix='class', $options = array(), &$output = array()) {
  static $disabled_modules = array();

  // Folder has been allready scaned (maybe for an other $prefix)
  // and detected as a folder of a disabled module.
  if ($disabled_modules[$directory]) {
    return array();
  }

  // Select 'file' or 'folder' mode
  $mode = (isset($options['mode'])) ? $options['mode'] : 'file';

  $directory_content = scandir($directory);

  // First pass, detect disabled modules,
  // check if dir contain a .module and a .info
  // Sometimes modules can be placed into a parent module
  // if children is not enabled, we don't have to scan this directory
  foreach ($directory_content as $directory_element) {
    $info = pathinfo($directory_element);
    if ($info['extension'] == 'module' && !module_exists($info['filename'])) {
      // This dir contain a disabled module
      $disabled_modules[$directory] = TRUE;
      return array();
    }
  }

  foreach ($directory_content as $directory_element) {
    if ($directory_element != '.' && $directory_element != '..') {

      if ((($mode == 'file' && is_file($directory . $directory_element)) || ($mode == 'folder' && is_dir($directory . $directory_element))) && drupal_substr($directory_element, 0, drupal_strlen($prefix . '.')) === $prefix . '.' && we_register_scan_dir_check_extension($directory_element, $options)) {
        $exp = explode('.', $directory_element);
        $output[$exp[1]] = $directory . $directory_element;
      }

      if (is_dir($directory .  $directory_element)) {
        we_register_scan_dir($directory . $directory_element . '/', $prefix, $options, $output);
      }
    }
  }

  return $output;
}

function we_register_scan_dir_check_extension($directory_element, $options) {

  // If no extension specified
  if (!isset($options['extensions']) || empty($options['extensions'])) {
    return TRUE;
  }

  // If match with a specified extension
  foreach ($options['extensions'] as $extension) {
    if (drupal_substr($directory_element, -drupal_strlen('.' . $extension)) === '.' . $extension) {
      return TRUE;
    }
  }

  return FALSE;
}