/**
 * WE Devel | Javascript inspection tool | v1.1
 * Romain WEEGER 2010 / 2011
 * 
 * Used to display content of javascript objects in an overlay window
 * using the d() function. Based on Krumo style.
 * Dev console is not a WE function or object style, to improve debuging in case of
 * WE javascript fails ;)
 */

//___________________ I N I T

we_devel_console = false;                       // jQuery object of we_devel_console DOM element
we_devel_console_opener = false;                // Open / Close button
we_devel_console_opened = false;
we_devel_console_lines = false;                 // jQuery object of we_develel_console_lines DOM element
we_devel_console_lines_counter = 0;           // Used to generate lines ids
we_devel_console_group_counter = 0;             // Used to generate groups ids
we_devel_console_groups = {};                   // List of groups
we_devel_console_area = false;

$(document).ready(function(){
  we_dev_console_init();
});

//___________________ F U N C T I O N S

function a(message) {
	alert(message);
}

function c(message) {
	console.log(message);
}

/**
 * Main function d();
 * Show javascript content value into we_devel_console
 */
function d(content,group,progressive) {
  
  we_dev_console_init();
	
  progressive = (progressive==undefined) ? true : progressive;

  if (typeof(group)=='string') {
    if (typeof(we_devel_console_groups[group])=='undefined') {
      we_devel_console_groups[group] = new we_devel_console_group(group);
      we_devel_console_lines.prepend(we_devel_console_groups[group].render());
      we_devel_console_groups[group].activate();
    }

    group = we_devel_console_groups[group];
  }

  var line = new we_devel_console_line(content,group,progressive);
  var output = line.render();

  if (group) {
    group.$childs.prepend(output);
  }
  else {
    // Render content, Add to console
    we_devel_console_lines.prepend(output);
  }
  
  // Activate line, and childs lines too if not progressive
  line.activate();

}

function we_dev_console_init() {
  
  if (we_devel_console!=false) {
    return;
  }
  
  // Add dev console source
  $('body').append('<div id="we_devel_console_area" style="display:none"><a id="we_devel_console_opener" class="we_devel_console_opener" href="javascript:void(0);">&nbsp;</a></div><div id="we_devel_console" class="we_devel_console"><div id="we_devel_console_header"><form id="we_devel_console_form_eval" name="we_devel_console_form_eval"><input id="we_devel_console_form_eval_value" type="text" name="eval_value" value="" /><input id="we_devel_console_form_eval_submit" type="submit" name="eval_submit" value="Inspect" /><input id="we_devel_console_form_eval_clear" type="submit" name="eval_clear" value="Clear" /></form><a href="javascript:void(0);" class="we_devel_console_opener" id="we_devel_console_close">&nbsp;</a></div><div id="we_devel_console_lines"></div></div>');
  we_devel_console = $('#we_devel_console');
  we_devel_console_lines = $('#we_devel_console_lines');
  we_devel_console_opener = $('#we_devel_console_opener');
  we_devel_console_area = $('#we_devel_console_area');

  $('#we_devel_console_close').click(we_devel_console_toggle);
  $('#we_devel_console_opener').click(we_devel_console_toggle);

  // Position
  we_devel_console
    .css('bottom','0')
    .css('right','0');

  $(window).bind('resize',we_devel_console_resize);

  // Submit by pressing "Enter"
  $('#we_devel_console_form_eval')
    .attr('autocomplete','off')
    .submit(we_devel_console_submit_inspect);

  // Submit by click
  $('#we_devel_console_form_eval_submit').click(we_devel_console_submit_inspect);

  // Clear
  $('#we_devel_console_form_eval_clear').click(we_devel_console_clear);

  we_devel_console_resize();

  we_devel_console_opened = eval(jQuery(document).get_cookie('we_devel_console_default_opened'));
  we_devel_console_toggle(we_devel_console_opened);
}

/**
 * Called when windows size changes
 */
function we_devel_console_resize() {
	var height = $(window).height();
	var height_dev = height*.6;
	we_devel_console.css('height',height_dev);
	we_devel_console_lines.css('height',height_dev-40);
}

/**
 * Open / Close console
 */
function we_devel_console_toggle(open) {
	var display = we_devel_console.css('display');
	if (display=='none' || open==true) {
		we_devel_console_area.css('display','none');
		we_devel_console.css('display','');
		we_devel_console_opened = true;
	}
	else {
		we_devel_console_area.css('display','');		
		we_devel_console.css('display','none');
		we_devel_console_opened = false;
	} 
	
	jQuery(document).set_cookie('we_devel_console_default_opened',we_devel_console_opened);

}

/**
 * Called when click on "Inspect" button
 */
function we_devel_console_submit_inspect() {
	var val = $('#we_devel_console_form_eval_value').val();
	$('#we_devel_console_form_eval_value').val('');
	
	// Execute asynchronously to handle error messages
	// and always block form submission
	setTimeout(function() {
		we_devel_console_inspect(val);
	},100);
	
	return false;
}

/**
 * Called by form submit timeout
 */
function we_devel_console_inspect(val) {

	var type = eval('typeof(' + val + ');');

	if (type=='undefined') {
		d(val);
	}
	else { 
		eval('var eval_val = ' + val + '; ');
		d(eval_val,null,true);
	}
	
	return false;
}

/**
 * Called when click on "Clear" button
 * Empty console content
 */
function we_devel_console_clear(val) {
	we_devel_console_lines_counter = 0;
	we_devel_console_group_counter = 0;
	we_devel_console_groups = {};
	we_devel_console_lines.html('');
	return false;
}

//___________________ C L A S S E S

/**
 * Default line displayed
 */
we_devel_console_line = function(content,group_name,progressive) {
	this.content = content;
	this.content_type = typeof(this.content);
	this.group_name = (group_name!=undefined) ? group_name : false;
	this.progressive = (progressive!=undefined) ? progressive : false;
	this.name = '';
	this.id = 'we_devel_console_line_' + we_devel_console_lines_counter++;
	this.title_id = false;
	this.childs_id = false;
	this.childs = [];
	this.childs_rendered = false;
	this.rendered = false;
	this.rendered_objects = [];
	this.parent_index = null;
};

we_devel_console_line.prototype = {
		
	render:function () {
	  var output = '';
	  
	  // Check if rendered
		for (var i in this.rendered_objects) { 
			if (this.rendered_objects[i]===this.content) {
				this.rendered = true;
			}
		}
	  
	  switch (this.content_type) {
			case 'string' :
			case 'number' :
			case 'boolean' :
				output = this.render_string(this.content,name);
				break;
			case 'object' :
				if (this.content==null) { 
					output = this.render_null(this.content,name);
				}
				else {
					this.rendered_objects.push(this.content);
					output = this.render_object(this.content,name);
				} 
				break;
			case 'function' :
				output = this.render_function(this.content,name);
				break;
		}
	  
		return output;
	},
	
	activate:function() {
		
		this.$ = $('#' + this.id);
		var activated = (this.$.hasClass('collapsed') || this.$.hasClass('expanded'));
		
		if (!activated && this.title_id && this.childs_id) {
			this.$title = $('#' + this.title_id);
			this.$childs = $('#' + this.childs_id);
			this.$.addClass('collapsed');
			
			var callback_this = this;
			this.$title.click(function() {
				callback_this.toggle();
			});
		}
		
		for (var i in this.childs) {
			this.childs[i].activate();
		}
		
		// Add dom content of highlighted source code after contaner is inserted in document
		// Because it's not simple to covert DOM to String directly on every browser.
		if (this.content_type=='function' && this.id_func_source!=undefined && this.render_function_dom!=undefined) {
			$('#' + this.id_func_source).html(this.render_function_dom);
		}
	
	},
	
	//______________ R E N D E R  T Y P E S

  render_string:function() {
		
		if (this.content.toString().length<20) {
			return this.render_line();
		}
		
		var output = '<div class="we_devel_group">';
		var save = this.content;
		this.content = this.content.substring(0,20) + '...';
		output += this.render_line_title();
		this.content = save;
		output += this.render_line_childs('<textarea>' + this.content + '</textarea>');
		output += '</div>';
		
		return output;
	},
	
	render_object:function() {
		var output = '<div class="we_devel_group">';
		
		output += this.render_line_title();
		
		if (!this.rendered) {
			
			if (!this.progressive) {
				output += this.render_line_childs(this.render_line_childs_content());
			}
			else {
				output += this.render_line_childs();
			}
		}
		
		output += '</div>';
		
		return output; 
	},
	
	render_null:function() {
		return this.render_line();
	},
	
	render_function:function() {
		var output = '';
		
		output += '<div class="we_devel_group">';
		output += this.render_line_title('',this.content);
			var originalTags = [];
			var inputString = this.content.toString();
			var highlightTags = sh_highlightString(inputString, sh_languages['javascript']);
		  var tags = sh_mergeTags(originalTags, highlightTags);
		  var documentFragment = sh_insertTags(tags, inputString);
		  this.render_function_dom = documentFragment;
		  
		  this.id_func_source = this.id + '_function_source';
		  output += this.render_line_childs('<pre id="' + this.id_func_source + '" class="sh_sourceCode"></pre>');
	  output += '</div>';
	  
		return output;
	},
	
	//______________ G L O B A L
	
	render_line:function() {
		var output = '';
		var string = (this.content_type=='string') ? this.content : '...';
		var typeUc = this.content_type.charAt(0).toUpperCase();
		typeUc += this.content_type.substr(1);
		
		var info = '';
		var string = '';
		
		switch (this.content_type) {
		
		  case 'string' :
		  	info = ', ' + this.content.length + ' characters';
		  	string = this.content;
			  break;
			  
		  case 'number' :
		  case 'boolean' :
		  	string = this.content;
			  break;
			  
		  case 'object' :

		  	if (this.content==null) {
		  		string = '<i style="font-weight:normal">NULL</i>';
		  	}
		  	else {
		  	    // Check if it's not an array
		  		if (this.content.constructor!=null && this.content.constructor.toString().indexOf("Array") == -1) {
				  	var length = 0;
				  	var class_name = /(\w+)\(/.exec(this.content.constructor.toString());
				  	
				  	for (var i in this.content) {
				  		length++;
				  	}

				  	this.name = (this.parent_index!=null) ? this.parent_index :  this.content.toString();

				}
				else {
					var length = this.content.length;
					typeUc = 'Array';
			  	}
			  	
			  	info = ', ' + length + ' elements';
			  	
			  	if (this.rendered) {
			  		string = '<span style="font-size:18px; vertical-align:top;">&#8734;</span> Recursion';
			  	}
			  	else {
			  		string = '...';
			  	}
		  	}
		  	
		  	break;
		  	
		  case 'function' :
		  	string = '...';
		  	break;
		}
		
		info = '(' + typeUc + info + ')';

		output += '<div id="' + this.id + '" class="we_devel_line we_devel_line_' + this.content_type + '">';
		output +=   (this.name!=undefined) ? '<span class="we_devel_line_info_name">' + this.name + '</span>' : '';
		output +=   '<span class="we_devel_line_info_type">' + info + '</span>';
		output +=   '<span class="we_devel_line_info_string">' + string + '</span>';
		output +=   '<span class="we_devel_line_info_time">' + this.time_get() + '</span>';
		output += '</div>';
		
		return output;
	},
	
	render_line_title:function () {
		this.title_id = this.id + '_title';
		return '<div id="' + this.title_id + '" class="we_devel_group_title">' + this.render_line() + '</div>';
	},
	
	render_line_childs:function(content) {
		var output = '';
		
		this.childs_id = this.id + '_childs';
		output += '<div id="' + this.childs_id + '" class="we_devel_line_object_childs" style="display:none;">';
		output += content;
		output += '</div>';
		
		return output;
	},
	
	render_line_childs_content:function() {
		var output = '';
		
		if (this.content==window || this.content==document) { return 'Unable to show content'; }
		
		for (var i in this.content) { 
			var line = new we_devel_console_line(this.content[i],this,this.progressive);
			line.name = i;
			line.rendered_objects = this.rendered_objects;
			line.parent_index = i;
			this.childs.push(line);
			output += line.render();
		}
		
		this.childs_rendered = true;
		return output;
	},
	
	// ____ U T I L S
	
	time_get:function() {
		var now = new Date();

		var hh = this.time_zero(now.getHours());
		var ii = this.time_zero(now.getMinutes());
		var ss = this.time_zero(now.getSeconds());
		
		return hh + ':' + ii + ':' + ss;
	},
	
	/**
	 * Add a zero before a number below 10
	 */
	time_zero:function(num) {
		num = num.toString();
		return (num.length<=1) ? '0' + num : num;
	}
};

/**
 * Render grouped lines
 */
we_devel_console_group = function(name) {
	this.name = name;
	this.id = 'we_devel_console_line_' + we_devel_console_group_counter++;
	this.id_title = this.id + '_title';
	this.id_childs = this.id + '_childs';
};

we_devel_console_group.prototype = {
	
	render:function() {
		var output = '';
		output += '<div id="' + this.id + '" class="we_devel_group">'
					  + '<div class="we_devel_group_title" id="' + this.id_title + '">'
					  	  + '<div class="we_devel_line">' + this.name + '</div>'
					  + '</div>'
					  + '<div class="we_devel_line_object_childs" style="display:none" id="' + this.id_childs + '"></div>'
				  + '</div>';
		return output;
	},
	
	activate:function() {
		this.$ = $('#' + this.id);
		this.$title = $('#' + this.id_title);
		this.$childs = $('#' + this.id_childs);
		
		this.$.addClass('collapsed');
		
		var callback_this = this;
		this.$title.click(function() {
			callback_this.toggle();
		});
	},
	
};

/**
 * toggle method is common to both objects types
 */
we_devel_console_line.prototype.toggle = 
we_devel_console_group.prototype.toggle = function() {
	var collapsed = (this.$.hasClass('collapsed')) ? true : false;
	
	if (this.content_type=='object' && !this.childs_rendered) {
		var output_childs = this.render_line_childs_content();
		this.$childs.html(output_childs);
		this.activate();
	}
	
	if (collapsed) {
		this.$.removeClass('collapsed').addClass('expanded');
		this.$childs.css('display','');
	}
	else {
		this.$.removeClass('expanded').addClass('collapsed');
		this.$childs.css('display','none');
	}
};