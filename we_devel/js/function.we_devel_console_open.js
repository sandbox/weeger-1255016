return function(message) {
  $we.load_jquery('add_js_link');
  $we.load_jquery('add_css_link');
  jQuery(document).add_js_link($we.url_module('we_devel') + '/js/we_devel.js');
  jQuery(document).add_js_link($we.url_module('we_devel') + '/js/sh_javascript.js');
  jQuery(document).add_js_link($we.url_module('we_devel') + '/js/shjs.min.js');
  jQuery(document).add_css_link($we.url_module('we_devel') + '/css/we_devel.css');
  jQuery(document).add_css_link($we.url_module('we_devel') + '/css/shjs.css');
  we_devel_console_toggle(true);
  d(message);
}