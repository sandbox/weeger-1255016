return function() {
	// Animate Druplicon
	Drupal.settings.we_druplicon_lookup.$_druplicon_lookup
	  .attr('alt','')
	  .attr('src','')
	  .css('padding','0')
	  .css('margin','20px 20px 0 0')
	  .we_animated_png({
		url:Drupal.settings.we_druplicon_lookup.image_url,
		direction:1,
		repeat:false,
		width:64,
		height:73,
		frames:1,
		frame_start:-30,
	  });

	$(document).mousemove(function(e) {
		
		var frame = e.pageX;
		var left = Drupal.settings.we_druplicon_lookup.$_druplicon_lookup.offset().left;
		
		if (frame<left) {
			frame = frame/left * 26;
		}
		else {
			frame = 26 + frame/100;
		}
		
		frame = Math.round(frame);
		frame = (frame>50) ? 50 : frame;
		frame = (frame<2) ? 2 : frame;
		frame = -frame;
		
		Drupal.settings.we_druplicon_lookup.$_druplicon_lookup.we_animated_png_start({
			frame:frame
		});
	});
}