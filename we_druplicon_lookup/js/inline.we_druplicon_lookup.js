$we.load_class('we_animated_png');

Drupal.settings.we_druplicon_lookup.$_druplicon_lookup = $('#logo');
Drupal.settings.we_druplicon_lookup.$_druplicon_lookup
  .attr('href','javascript:void(0);')
  .click(function() {
	$we.load_image(Drupal.settings.we_druplicon_lookup.image_url,function() {
		$we._call('function','we_druplicon_lookup_init');
	});
	return false;
});